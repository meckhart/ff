﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;


namespace Einsatz_Karte.Converter
{
    public class KMConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                double km = System.Convert.ToDouble(value, CultureInfo.InvariantCulture);
                if(km==0)
                    return "kein Einsatz";
                return km + " km bis zum Einsatzort";
            }
            else
            {
                return "kein Einsatz";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}