﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Einsatz_Karte.Converter
{
    public class TimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                TimeSpan t = (TimeSpan)value;
                if (t.Seconds==0)
                    return "kein Einsatz";
                return String.Format("Zeit bis zum Einsatzort: {0,2:D2}:{1,2:D2}:{2,2:D2}", t.Hours, t.Minutes, t.Seconds);
            }
            else
            {
                return "kein Einsatz";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}