﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Einsatz_Karte.CustomControls
{
    public sealed partial class InputMessageDialog : UserControl
    {
        private string m_O1;
        private string m_O2;

        public ObservableCollection<String> OrigAddress;

        private TaskCompletionSource<bool> m_TaskCompletionSource;
        public InputMessageDialog(string o1, string o2)
        {
            m_O1 = o1;
            m_O2 = o2;
            OrigAddress = new ObservableCollection<string>();

            if (m_O1 == null)
                m_O1 = "";
            if (m_O2 == null)
                m_O2 = "";
            this.InitializeComponent();
        }

        public InputMessageDialog(string o1, string o2, string orgiAddress)
        {
            m_O1 = o1;
            m_O2 = o2;

            if (m_O1 == null)
                m_O1 = "";
            if (m_O2 == null)
                m_O2 = "";

            orgiAddress = orgiAddress.Replace(":", " : ");
            orgiAddress = orgiAddress.Replace(";", " ; ");
            orgiAddress = orgiAddress.Replace("-", " - ");
            orgiAddress = orgiAddress.Replace("_", " _ ");
            orgiAddress = orgiAddress.Replace("@", " @ ");
            orgiAddress = orgiAddress.Replace("   ", " ");
            orgiAddress = orgiAddress.Replace("  ", " ");
            OrigAddress = new ObservableCollection<string>(orgiAddress.Split(' '));
            

            this.InitializeComponent();

            foreach (string s in orgiAddress.Split(' '))
            {
                Button b = new Button();
                b.Content = s;
                b.MinWidth = 50;
                b.Click+=OnAddText;
                OrgiAddressPanel.Children.Add(b);
            }
        }

        private void OnAddText(object sender, RoutedEventArgs e)
        {
            String textToAdd = ((Button)sender).Content.ToString();
            string text = m_TextBox2.Text;
            if (text == null)
            {
                text = "";
            }
            if (text.EndsWith(" ")|| text.Length==0)
            {
                text += textToAdd;
            }
            else
            {
                text += " " + textToAdd;
            }
            m_TextBox2.Text = text;
        }

        public Task<bool> ShowAsync()
        {
            InitFields();
            m_Popup.IsOpen = true;
            m_TaskCompletionSource = new TaskCompletionSource<bool>();
            return m_TaskCompletionSource.Task;
        }

        public void InitFields()
        {
            m_Rect1.Height = Window.Current.Bounds.Height;
            m_Rect1.Width = Window.Current.Bounds.Width;
            m_Rect2.Width = Window.Current.Bounds.Width;
            m_TextBox1.Width = Window.Current.Bounds.Width / 2;
            m_TextBox1.Text = m_O1;
            m_TextBox2.Width = Window.Current.Bounds.Width / 2;
            m_TextBox2.Text = m_O2;
        }

        public TextBox TextBox
        {
            get { TextBox b = new TextBox(); b.Text = m_TextBox1.Text + "&" + m_TextBox2.Text; return b; }
        }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            m_TaskCompletionSource.SetResult(true);
            m_Popup.IsOpen = false;
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            m_TaskCompletionSource.SetResult(false);
            m_Popup.IsOpen = false;
        }

        private void AppBarButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            m_TextBox2.Text = "";
        }

        private void AppBarButton_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            OkButton.Focus(FocusState.Programmatic);
        }
    }
}
