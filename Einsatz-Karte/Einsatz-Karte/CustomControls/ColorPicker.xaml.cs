﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Einsatz_Karte.CustomControls
{
    public sealed partial class ColorPicker : UserControl
    {
        public Color MyColor { get; set; }
        private TaskCompletionSource<Color> m_TaskCompletionSource;
        public List<CustomColors> ColorBrushes { get; set; }
        public ColorPicker()
        {
            this.InitializeComponent();
            ColorBrushes = new List<CustomColors>();
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Red)});
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Blue) });
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Yellow) });
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Green) });
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Brown) });
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Black) });
            ColorBrushes.Add(new CustomColors { ColorBrush = new SolidColorBrush(Colors.Purple) });
            itemGridView.ItemsSource = ColorBrushes;
        }

        public Task<Color> ShowAsync()
        {
            InitFields();
            m_Popup.IsOpen = true;
            m_TaskCompletionSource = new TaskCompletionSource<Color>();
            return m_TaskCompletionSource.Task;
        }

        public void InitFields()
        {
            m_Rect1.Height = Window.Current.Bounds.Height;
            m_Rect1.Width = Window.Current.Bounds.Width;
            m_Rect2.Width = Window.Current.Bounds.Width;
        }


        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Grid g = (Grid)sender;
            SolidColorBrush b = (SolidColorBrush)g.Background;
            Color c = b.Color;
            c.A = 125;
            m_TaskCompletionSource.SetResult(c);
            m_Popup.IsOpen = false;
        }
    }

    public class CustomColors
    {
        private SolidColorBrush _ColorBrush;

        public SolidColorBrush ColorBrush
        {
            get { return _ColorBrush; }
            set { _ColorBrush = value; }
        }
        
    }
}
