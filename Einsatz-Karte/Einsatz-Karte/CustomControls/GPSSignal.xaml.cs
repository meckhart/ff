﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Einsatz_Karte.CustomControls
{ 
    public sealed partial class GPSSignal : UserControl
    {
        private List<Ellipse> _Shapes;
        private int _Count;
        public GPSSignal()
        {
            this.InitializeComponent();
            _Shapes = new List<Ellipse>();
            _Count = 18;
            Move.Completed+=Move_Completed;
            Move.Begin();
        }

        private void Move_Completed(object sender, object e)
        {
            //await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(50));
            if (_Count == 18)
            {
                Ellipse el = new Ellipse();
                el.Fill = new SolidColorBrush(Colors.Transparent);
                el.Stroke = new SolidColorBrush(Colors.Red);
                el.Height = 3;
                el.Width = 3;
                el.StrokeThickness = 1.5f;
                Canvas.SetLeft(el, -1);
                Canvas.SetTop(el, -1);
                _Shapes.Add(el);
                LayoutRoot.Children.Add(el);
                _Count = 0;
            }
            else
            {
                _Count++;
            }

            foreach (Ellipse el in _Shapes)
            {
                Canvas.SetTop(el, Canvas.GetTop(el) - 0.25f);
                Canvas.SetLeft(el, Canvas.GetLeft(el) - 0.25f);
                el.Width = el.Width + 0.5f;
                el.Height = el.Height + 0.5f;
            }

            for (int i = 0; i < _Shapes.Count; i++)
            {
                if (Canvas.GetTop(_Shapes[i]) < -12)
                {
                    LayoutRoot.Children.Remove(_Shapes[i]);
                    _Shapes.RemoveAt(i);
                    i--;
                }
            }
            Move.Begin();

        }
    }
}
