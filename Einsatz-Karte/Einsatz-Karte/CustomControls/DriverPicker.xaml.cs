﻿using Einsatz_Karte.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Einsatz_Karte.CustomControls
{
    public sealed partial class DriverPicker : UserControl
    {
        private TaskCompletionSource<Firefighter> m_TaskCompletionSource;
        private ObservableCollection<Firefighter> _Drivers;

        public ObservableCollection<Firefighter> Drivers
        {
            get { return _Drivers; }
            set { _Drivers = value; }
        }
        
        private Firefighter Driver;
        public DriverPicker()
        {
            this.InitializeComponent();

        }

        public Task<Firefighter> ShowAsync(Vehicle v)
        {
            InitFields(v);
            m_Popup.IsOpen = true;
            m_TaskCompletionSource = new TaskCompletionSource<Firefighter>();
            return m_TaskCompletionSource.Task;
        }

        public async void InitFields(Vehicle v)
        {
            m_Rect1.Height = Window.Current.Bounds.Height;
            m_Rect1.Width = Window.Current.Bounds.Width;
            m_Rect2.Width = Window.Current.Bounds.Width;

            //IEnumerable<Firefighter> ie = await Einsatz_Karte.Service.FileService.GetFirefighterXML();
            IEnumerable<Firefighter> ie = Einsatz_Karte.Service.ReportService.Team;
            Drivers = new ObservableCollection<Firefighter>();
            foreach(Firefighter ff in ie)
            {
                if(ff.VehiclesAllowed!=null && ff.IsTapped ==true)
                {
                    if(ff.VehiclesAllowed.Contains(v.Name))
                    {
                        Drivers.Add(ff);
                    }
                }
            }
            itemGridView.ItemsSource = Drivers;
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Grid g = (Grid)sender;
            String imgPaht = g.Tag.ToString();
            Driver = Drivers.First(p => p.ImageUrl.Equals(imgPaht));
            m_TaskCompletionSource.SetResult(Driver);
            m_Popup.IsOpen = false;
        }

        private void AppBarButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            m_TaskCompletionSource.SetResult(null);
            m_Popup.IsOpen = false;
        }
    }
}
