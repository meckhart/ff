﻿using Einsatz_Karte.Model;
using Limilabs.Mail.MIME;
using PdfSharp.Pdf;
using PdfSharp.Pdf.Content;
using PdfSharp.Pdf.Content.Objects;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;



namespace Einsatz_Karte.Service
{
    public class FileService
    {
        public static string LocalFolderUrl;

        static FileService()
        {
            SetPath();
        }

        private static async void SetPath()
        {
            LocalFolderUrl = ApplicationData.Current.LocalFolder.Path;
        }

        public static async Task<string[]> GetPdfAlertLocation()
        {
            IEnumerable<string> text = await GetPdfTextAsync();
            string [] alertAddress = GetAddress(text);
            return alertAddress;
        }

        public static async Task<Report> GetReportFromPdf()
        {
            Report rep = new Report();
            IEnumerable<string> text = await GetPdfTextAsync();
            String[] textArray = text.ToArray<String>();
            string alertAddress = GetAddress(text)[0];
            //Set UserLoc and UseStreet
            if (Regex.IsMatch(alertAddress, @"^\d"))
            {
                string[] help = alertAddress.Split(' ');
                if (help.Length > 1)
                {
                    rep.UseLocation = help[0] + " " + help[1];
                    string street = "";
                    if (help.Length > 2)
                    {
                        for (int i = 2; i < help.Length; i++)
                        {
                            street += help[i] + " ";
                        }
                        rep.UseStreet = street;
                    }
                }
            }
            else
            {
                string[] help = alertAddress.Split(' ');
                if (help.Length > 0)
                {
                    rep.UseLocation = help[0];
                    string street = "";
                    if (help.Length > 1)
                    {
                        for (int i = 1; i < help.Length; i++)
                        {
                            street += help[i] + " ";
                        }
                        rep.UseStreet = street;
                    }
                }
            }

            //Set Date
            int index = Array.IndexOf(textArray, "Zusatzinfo:");
            if(index>0)
            {
                string help = textArray[index+1];
                DateTime dt = new DateTime();
                help=help.Replace(" - ", " ");
                bool b = DateTime.TryParse(help, out dt);
                if(b)
                {
                    rep.Date = dt;
                    rep.AlertTime = dt;
                }

            }

            //Set Owner
            index = Array.IndexOf(textArray, "Gemeldet über:");
            if (index > 0)
            {
                rep.Owner = textArray[index + 1];
                rep.OwnerStreet = textArray[index + 3];
                rep.OwnerLocation = textArray[index + 6];
            }

            return rep;
        }

        private static string [] GetAddress(IEnumerable<string> text)
        {
            bool b = false;
            string address = "";
            string orgiaddress = "";
            foreach(string s in text)
            {
                if (s == "Einsatzort:")
                    b = false;
                if(b==true)
                {
                    address = address + s;
                }
                if (s == "Einsatzdaten:")
                    b = true;        
            }
            orgiaddress = address;
            if(address.Contains("-OT"))
            {
                int index = address.IndexOf("-OT");
                address = address.Substring(0, index);
                var help = address.Split(' ');
                address = help[help.Length - 1]+" ";
                for (int i = 0; i < help.Length - 2; i++)
                    address = address + help[i]+" ";
            }
            return new string[] { address.ToLower(), orgiaddress };
        }

        private static async Task<IEnumerable<string>> GetPdfTextAsync()
        {
            StorageFile sf = await StorageFile.GetFileFromPathAsync(GlobalVariablesService.AlertPdfPath);
            PdfDocument d = PdfReader.Open(sf.Path);
            PdfPage pp = d.Pages[0];
            var content = ContentReader.ReadContent(pp);
            var text = ExtractText(content);
            return text;
        }

        private static IEnumerable<string> ExtractText(CObject cObject)
        {
            var textList = new List<string>();
            if (cObject is COperator)
            {
                var cOperator = cObject as COperator;
                if (cOperator.OpCode.Name == OpCodeName.Tj.ToString() ||
                    cOperator.OpCode.Name == OpCodeName.TJ.ToString())
                {
                    foreach (var cOperand in cOperator.Operands)
                    {
                        textList.AddRange(ExtractText(cOperand));
                    }
                }
            }
            else if (cObject is CSequence)
            {
                var cSequence = cObject as CSequence;
                foreach (var element in cSequence)
                {
                    textList.AddRange(ExtractText(element));
                }
            }
            else if (cObject is CString)
            {
                var cString = cObject as CString;
                textList.Add(cString.Value);
            }
            return textList;
        }


        public static async Task<bool> SavePDF(MimeData md)
        {
            StorageFolder appFolder = await ApplicationData.Current.LocalFolder.GetFolderAsync("Alerts");
            String filename=DateTime.Now.ToString();
            filename=filename.Replace(" ", "_");
            filename = filename.Replace(":", "");
            filename = filename.Replace(".", "");
            filename += ".pdf";
            StorageFile sf = await appFolder.CreateFileAsync(filename);
            await md.Save(sf);
            GlobalVariablesService.AlertPdfPath = sf.Path;
            return true;
            //try
            //{
            //    sf= await appFolder.GetFileAsync("Alert.pdf");              
            //}
            //catch
            //{
            //    CreatePDFFile(md);
            //    return;
            //}
            //try
            //{
            //    await md.Save(sf);
            //}
            //catch { }
        }

        private async static void CreatePDFFile(MimeData md)
        {
            StorageFolder appFolder = ApplicationData.Current.LocalFolder;
            StorageFile sf = await appFolder.CreateFileAsync("Alert.pdf", CreationCollisionOption.ReplaceExisting);
            SavePDF(md);
        }     

        public async static Task<bool> SaveHaydrantsXML(IEnumerable<Hydrant> hy)
        {
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Hydrants.xml");
            using (var stream = await sf.OpenAsync(FileAccessMode.ReadWrite))
            {
                XmlSerializer serializer = new XmlSerializer(hy.GetType());
                serializer.Serialize(stream.AsStreamForWrite(), hy);
                await stream.FlushAsync();
                stream.Size = stream.Position;
            }
            return true;
        }

        public async static Task<IEnumerable<Hydrant>> GetHydrantsXML()
        {
            try
            {
                IStorageFile sf = await ApplicationData.Current.LocalFolder.TryGetItemAsync("Hydrants.xml") as IStorageFile;
                if (sf == null)
                {
                    return Enumerable.Empty<Hydrant>();
                }
                XmlSerializer reader = new XmlSerializer(typeof(List<Hydrant>));
                Stream s =  sf.OpenStreamForReadAsync().Result;
                using (System.IO.StreamReader file = new System.IO.StreamReader(s))
                {
                    IEnumerable<Hydrant> h = (IEnumerable<Hydrant>)reader.Deserialize(file);
                    return h;
                }
            }
            catch (Exception e)
            { return null; }
        }

        public async static void OpenAlarmFax()
        {
            try
            {
                StorageFile sf = await StorageFile.GetFileFromPathAsync(GlobalVariablesService.AlertPdfPath);
                await Windows.System.Launcher.LaunchFileAsync(sf);
            }
            catch { }
        }

        public async static Task<bool> SaveSettlementsXML(IEnumerable<Settlement> se)
        {
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Settlements.xml");
            using (var stream = await sf.OpenAsync(FileAccessMode.ReadWrite))
            {
                XmlSerializer serializer = new XmlSerializer(se.GetType());
                serializer.Serialize(stream.AsStreamForWrite(), se);
                await stream.FlushAsync();
                stream.Size = stream.Position;
            }
            return true;
        }

        public async static Task<IEnumerable<Settlement>> GetSettlementsXML()
        {
            try
            {
                IStorageFile sf = await ApplicationData.Current.LocalFolder.TryGetItemAsync("Settlements.xml") as IStorageFile;
                if (sf == null)
                {
                    return Enumerable.Empty<Settlement>();
                }
                XmlSerializer reader = new XmlSerializer(typeof(List<Settlement>));
                Stream s = sf.OpenStreamForReadAsync().Result;
                using (System.IO.StreamReader file = new System.IO.StreamReader(s))
                {
                    IEnumerable<Settlement> se = (IEnumerable<Settlement>)reader.Deserialize(file);
                    return se;
                }
            }
            catch (Exception e)
            { return null; }
        }

        public async static Task<bool> SaveConfigXML(IEnumerable<ConfigItem> con)
        {
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Config.xml");
            using (var stream = await sf.OpenAsync(FileAccessMode.ReadWrite))
            {
                XmlSerializer serializer = new XmlSerializer(con.GetType());
                serializer.Serialize(stream.AsStreamForWrite(), con);
                await stream.FlushAsync();
                stream.Size = stream.Position;
            }
            return true;
        }

        public async static Task<IEnumerable<ConfigItem>> GetConfigXML()
        {
            try
            {
                IStorageFile sf = await ApplicationData.Current.LocalFolder.TryGetItemAsync("Config.xml") as IStorageFile;
                if (sf == null)
                {
                    return Enumerable.Empty<ConfigItem>();
                }
                XmlSerializer reader = new XmlSerializer(typeof(List<ConfigItem>));
                Stream s = sf.OpenStreamForReadAsync().Result;
                using (System.IO.StreamReader file = new System.IO.StreamReader(s))
                {
                    IEnumerable<ConfigItem> conf = (IEnumerable<ConfigItem>)reader.Deserialize(file);
                    return conf;
                }
            }
            catch (Exception e)
            { return null; }
        }

        public async static Task<bool> SaveFirefighterXML(IEnumerable<Firefighter> ff)
        {
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Firefighters.xml");
            using (var stream = await sf.OpenAsync(FileAccessMode.ReadWrite))
            {
                XmlSerializer serializer = new XmlSerializer(ff.GetType());
                serializer.Serialize(stream.AsStreamForWrite(), ff);
                await stream.FlushAsync();
                stream.Size = stream.Position;
            }
            return true;
        }

        public async static Task<IEnumerable<Firefighter>> GetFirefighterXML()
        {
            try
            {
                IStorageFile sf = await ApplicationData.Current.LocalFolder.TryGetItemAsync("Firefighters.xml") as IStorageFile;
                if (sf == null)
                {
                    return Enumerable.Empty<Firefighter>();
                }
                XmlSerializer reader = new XmlSerializer(typeof(List<Firefighter>));
                Stream s = sf.OpenStreamForReadAsync().Result;
                using (System.IO.StreamReader file = new System.IO.StreamReader(s))
                {
                    IEnumerable<Firefighter> ff = (IEnumerable<Firefighter>)reader.Deserialize(file);
                    return ff;
                }
            }
            catch (Exception e)
            { return null; }
        }

        public async static Task<bool> SaveVehiclesXML(IEnumerable<Vehicle> vh)
        {
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Vehicle.xml");
            using (var stream = await sf.OpenAsync(FileAccessMode.ReadWrite))
            {
                XmlSerializer serializer = new XmlSerializer(vh.GetType());
                serializer.Serialize(stream.AsStreamForWrite(), vh);
                await stream.FlushAsync();
                stream.Size = stream.Position;
            }
            return true;
        }

        public async static Task<IEnumerable<Vehicle>> GetVehiclesXML()
        {
            try
            {
                IStorageFile sf = await ApplicationData.Current.LocalFolder.TryGetItemAsync("Vehicle.xml") as IStorageFile;
                if (sf == null)
                {
                    return Enumerable.Empty<Vehicle>();
                }
                XmlSerializer reader = new XmlSerializer(typeof(List<Vehicle>));
                Stream s = sf.OpenStreamForReadAsync().Result;
                using (System.IO.StreamReader file = new System.IO.StreamReader(s))
                {
                    IEnumerable<Vehicle> vh = (IEnumerable<Vehicle>)reader.Deserialize(file);
                    return vh;
                }
            }
            catch (Exception e)
            { return null; }
        }

        public async static Task<List<Town>> GetTownsXML()
        {
            try
            {
                List<Town> towns=new List<Town>();

                IStorageFile sf = await ApplicationData.Current.LocalFolder.TryGetItemAsync("Towns.xml") as IStorageFile;
                if (sf == null)
                {
                    return new List<Town>();
                }
                XmlDocument doc = new Windows.Data.Xml.Dom.XmlDocument();
                string xml = await Windows.Storage.FileIO.ReadTextAsync(sf, Windows.Storage.Streams.UnicodeEncoding.Utf8);
                doc.LoadXml(xml);

                XmlNodeList townslist = doc.GetElementsByTagName("relation");
                XmlNodeList wayslist = doc.GetElementsByTagName("way");
                foreach (var town in townslist)
                {
                    Town t = new Town();
                    t.Ways = new List<Way>();

                    foreach (var child in town.ChildNodes)
                    {
                        if (child.NodeName.ToString() == "member" &&  child.Attributes[2].NodeValue.ToString()=="outer")
                        {
                            double id = Convert.ToDouble(child.Attributes.First(p => p.NodeName == "ref").NodeValue);
                            List<double> refValues = new List<double>();
                            var way = wayslist.First(p => Convert.ToDouble(p.Attributes[0].NodeValue) == id);
                            foreach (var refNode in way.ChildNodes)
                            {
                                if (refNode.NodeName.ToString() == "nd")
                                {
                                    refValues.Add(Convert.ToDouble(refNode.Attributes[0].NodeValue));
                                }
                            }
                            t.Ways.Add(new Way(id, refValues));
                        }
                        else if (child.NodeName.ToString() == "tag")
                        {
                            switch(child.Attributes.First(p=>p.NodeName=="k").NodeValue.ToString())
                            {
                                case "administrative_title": t.Type = child.Attributes.First(p => p.NodeName == "v").NodeValue.ToString(); break;
                                case "name": t.Name= child.Attributes.First(p => p.NodeName == "v").NodeValue.ToString(); break;
                                case "postal_code": t.PostalCode= Convert.ToInt32(child.Attributes.First(p => p.NodeName == "v").NodeValue.ToString()); break;
                                    
                            }
                        }
                    }
                    towns.Add(t);
                }
                return towns;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async static Task<Dictionary<double, BorderPoint>> GetBorderPointsXML()
        {
            try
            {
                Dictionary<double, BorderPoint> borderPoints = new Dictionary<double, BorderPoint>();

                StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Towns.xml");
                XmlDocument doc = new Windows.Data.Xml.Dom.XmlDocument();
                string xml = await Windows.Storage.FileIO.ReadTextAsync(sf, Windows.Storage.Streams.UnicodeEncoding.Utf8);
                doc.LoadXml(xml);

                XmlNodeList borderPointslist = doc.GetElementsByTagName("node");
                foreach (var borderPoint in borderPointslist)
                {
                    BorderPoint point = new BorderPoint();
                    foreach(var attribute in borderPoint.Attributes)
                    {
                        switch(attribute.NodeName)
                        {
                            case "id": point.ID = Convert.ToDouble(attribute.NodeValue); break;
                            case "lat": point.Lat = Convert.ToDouble(attribute.NodeValue); break;
                            case "lon": point.Lon = Convert.ToDouble(attribute.NodeValue); break;
                        }
                    }
                    borderPoints.Add(point.ID,point);
                }

                return borderPoints;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static async Task<bool> SaveNewHydrantImage(Hydrant item, bool isDefault=false)
        {
            if (!isDefault)
            {
                FileOpenPicker openPicker = new FileOpenPicker();
                openPicker.ViewMode = PickerViewMode.Thumbnail;
                openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                openPicker.FileTypeFilter.Add(".png");
                openPicker.FileTypeFilter.Add(".jpeg");
                openPicker.FileTypeFilter.Add(".jpg");

                StorageFile file = await openPicker.PickSingleFileAsync();
                if (file != null)
                {
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(Einsatz_Karte.Service.FileService.LocalFolderUrl + "\\HydrantPics");
                    StorageFile newFile = await folder.CreateFileAsync(item.GUID + file.FileType, CreationCollisionOption.ReplaceExisting);
                    await file.CopyAndReplaceAsync(newFile);
                    string newPath = newFile.Path.Substring(newFile.Path.IndexOf("\\HydrantPics\\"));
                    item.ImageUrl = newPath;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    StorageFile file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync("Assets\\HydrantStandart.png");
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(Einsatz_Karte.Service.FileService.LocalFolderUrl + "\\HydrantPics");
                    StorageFile newFile = await folder.CreateFileAsync(item.GUID + file.FileType, CreationCollisionOption.ReplaceExisting);
                    await file.CopyAndReplaceAsync(newFile);
                    string newPath = newFile.Path.Substring(newFile.Path.IndexOf("\\HydrantPics\\"));
                    item.ImageUrl = newPath;
                    return true;
                }
                catch { return false; }
            }
        }

        public static async Task<bool> SaveNewSettlementImage(Settlement item, bool isDefault = false)
        {
            if (!isDefault)
            {
                FileOpenPicker openPicker = new FileOpenPicker();
                openPicker.ViewMode = PickerViewMode.Thumbnail;
                openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                openPicker.FileTypeFilter.Add(".png");
                openPicker.FileTypeFilter.Add(".jpeg");
                openPicker.FileTypeFilter.Add(".jpg");

                StorageFile file = await openPicker.PickSingleFileAsync();
                if (file != null)
                {
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(Einsatz_Karte.Service.FileService.LocalFolderUrl + "\\SettlementPics");
                    StorageFile newFile = await folder.CreateFileAsync(item.GUID + file.FileType, CreationCollisionOption.ReplaceExisting);
                    await file.CopyAndReplaceAsync(newFile);
                    string newPath = newFile.Path.Substring(newFile.Path.IndexOf("\\SettlementPics\\"));
                    item.ImageUrl = newPath;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    StorageFile file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync("Assets\\Settings\\Settlement.png");
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(Einsatz_Karte.Service.FileService.LocalFolderUrl + "\\SettlementPics");
                    StorageFile newFile = await folder.CreateFileAsync(item.GUID + file.FileType, CreationCollisionOption.ReplaceExisting);
                    await file.CopyAndReplaceAsync(newFile);
                    string newPath = newFile.Path.Substring(newFile.Path.IndexOf("\\SettlementPics\\"));
                    item.ImageUrl = newPath;
                    return true;
                }
                catch { return false; }
            }
        }

        public static async Task<bool> OpenFile(string path)
        {
            try
            {
                StorageFile file = await StorageFile.GetFileFromPathAsync(path);

                if (file != null)
                {
                    await Windows.System.Launcher.LaunchFileAsync(file);
                    return true;
                }
            }
            catch { }

            return false;
        }

        public static async Task<bool> DeleteUnnecessaryHydrantImages(List<Hydrant> items)
        {
            try
            {
                StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(Einsatz_Karte.Service.FileService.LocalFolderUrl + "\\HydrantPics");
                IReadOnlyList<StorageFile> files = await folder.GetFilesAsync();
                foreach (StorageFile file in files)
                {
                    if (!items.Cast<Hydrant>().Any(p => p.GUID == file.DisplayName))
                    {
                        await file.DeleteAsync();
                    }
                }
                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> DeleteUnnecessarySettlementsImages(List<Settlement> items)
        {
            try
            {
                StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(Einsatz_Karte.Service.FileService.LocalFolderUrl + "\\SettlementPics");
                IReadOnlyList<StorageFile> files = await folder.GetFilesAsync();
                foreach (StorageFile file in files)
                {
                    if (!items.Cast<Settlement>().Any(p => p.GUID == file.DisplayName))
                    {
                        await file.DeleteAsync();
                    }
                }
                return true;
            }
            catch { return false; }
        }
    }
}
