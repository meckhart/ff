﻿using Bing.Maps;
using Einsatz_Karte.Model.APIXML;
using Einsatz_Karte.Model.BingMaps;
using Einsatz_Karte.Model.CustomExecptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Windows.Data.Xml.Dom;

namespace Einsatz_Karte.Service
{
    public static class BingMapsService 
    {
        public static async Task<RoutePropertiesResponse> GetRouteLocations(Location loc1, Location loc2)
        {
            List<Location> routeLocs = new List<Location>();
            RouteResponse response = await GetRouteResponse(loc1, loc2);

            if (response.StatusDescription == "OK")
            {
                if (response.ResourceSets.ResourceSetCollection != null && response.ResourceSets.ResourceSetCollection.Count > 0)
                {
                    if (response.ResourceSets.ResourceSetCollection[0].Resources != null && response.ResourceSets.ResourceSetCollection[0].Resources.Count > 0)
                    {
                        if (response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection != null && response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection.Count > 0)
                        {
                            if (response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection != null && response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection.Count > 0)
                            {
                                if (response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection[0].Points != null)
                                {
                                    RoutePropertiesResponse rp = new RoutePropertiesResponse();
                                    foreach (Point p in response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection[0].Points)
                                    {
                                        rp.RouteLocations.Add(new Location(p.Latitude, p.Longitude));
                                    }
                                    rp.TravelDistance = response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].TravelDistance;
                                    rp.TravelDuration = response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].TravelDuration;
                                    return rp;
                                }
                            }
                        }
                    }
                }
            }
            else if (response.StatusDescription == "Not Found")
            {
                throw new RestException(RestExceptionType.NoRouteFound, "Es konnte keine Route gefunden werden!");
            }
            else 
            {
                throw new RestException(RestExceptionType.Common, "Ein allgemeiner Fehler ist aufgetaucht!("+response.StatusDescription + " : " +response.AuthenticationResultCode);
            }

            throw new RestException(RestExceptionType.NoRouteFound, "Es konnte keine Route gefunden werden!");
        }

        private static async Task<XmlDocument> GetXmlResponse(string requestUrl)
        {
            try
            {
                HttpClient myClient = new HttpClient();
                HttpResponseMessage msg = await myClient.GetAsync(requestUrl);
                XmlDocument doc = new XmlDocument();
                string s = await msg.Content.ReadAsStringAsync();
                doc.LoadXml(await msg.Content.ReadAsStringAsync());
                return doc;
            }
            catch
            {
                return null;
            }
        }

        private static Stream GenerateStreamFromString(string s)
        {

            s = s.Replace("xmlns=\"http://schemas.microsoft.com/search/local/ws/rest/v1\"", "");
            s = s.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
            s = s.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
            //s = s.Replace("Response   ", "Response");
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static async Task<double> GetRouteDistance(Location loc1, Location loc2)
        {
            RouteResponse response = await GetRouteResponse(loc1, loc2);
            if (response.StatusDescription == "OK")
            {
                if (response.ResourceSets.ResourceSetCollection != null && response.ResourceSets.ResourceSetCollection.Count > 0)
                {
                    if (response.ResourceSets.ResourceSetCollection[0].Resources != null && response.ResourceSets.ResourceSetCollection[0].Resources.Count > 0)
                    {
                        if (response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection != null && response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection.Count > 0)
                        {
                            if (response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection != null && response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection.Count > 0)
                            {
                                if (response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].RoutePath.RoutePathCollection[0].Points != null)
                                {
                                    return response.ResourceSets.ResourceSetCollection[0].Resources[0].RouteCollection[0].TravelDistance;
                                }
                            }
                        }
                    }
                }
            }
            else if (response.StatusDescription == "Not Found")
            {
                throw new RestException(RestExceptionType.NoRouteFound, "Es konnte keine Route gefunden werden!");
            }
            else
            {
                throw new RestException(RestExceptionType.Common, "Ein allgemeiner Fehler ist aufgetaucht!(" + response.StatusDescription + " : " + response.AuthenticationResultCode);
            }

            throw new RestException(RestExceptionType.NoRouteFound, "Es konnte keine Route gefunden werden!");
        }
        private static async Task<RouteResponse> GetRouteResponse(Location loc1, Location loc2)
        {
            if (ConfigService.BingMapsKey == null || ConfigService.BingMapsKey == "")
            {
                throw new RestException(RestExceptionType.NoKeyInConfig, "Es wurde kein Bing Maps Key in der Config Datei gefunden!");
            }

            String routeRequest = string.Format("http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0={0}&wp.1={1}&output=xml&rpo=Points&key={2}", loc1.Latitude + " " + loc1.Longitude, loc2.Latitude + " " + loc2.Longitude, ConfigService.BingMapsKey);
            XmlDocument routeResponse = await GetXmlResponse(routeRequest);
            if (routeResponse == null)
            {
                throw new RestException(RestExceptionType.NoConnection, "Der Bing Maps Dienst konnte nicht erreicht werden!");
            }

            XmlSerializer ser = new XmlSerializer(typeof(RouteResponse));

            RouteResponse response;
            using (Stream s = GenerateStreamFromString(routeResponse.GetXml()))
            {
                using (XmlReader reader = XmlReader.Create(s))
                {
                    response = (RouteResponse)ser.Deserialize(reader);
                }
            }
            return response;
        }
            
    }
}
