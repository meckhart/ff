﻿using Bing.Maps;
using Einsatz_Karte.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Service
{
    public static class TownService
    {
        public static List<Town> Towns;
        public static Dictionary<double, BorderPoint> BorderPoints;
        public static Dictionary<double, Way> Ways;

        public static bool IsLoaded;

        static TownService()
        {
            //LoadTownService();
            IsLoaded = false;
        }

        public static async Task<bool> LoadTownService()
        {
            Towns = await FileService.GetTownsXML();
            BorderPoints = await FileService.GetBorderPointsXML();
            Ways = new Dictionary<double, Way>();
            foreach (Town town in Towns)
            {
                foreach (Way way in town.Ways)
                {
                    if (!Ways.ContainsKey(way.ID))
                    {
                        Ways.Add(way.ID, way);
                    }

                }
            }
            return true;
        }
  
        public static List<Location> GetPolygonForTown(string name)
        {
            Town town = Towns.Find(p => p.Name == name);
            if(town !=null)
            {
                List<Location> locs = new List<Location>();
                List<List<Location>> allRoutes= new List<List<Location>>();
                allRoutes = new List<List<Location>>();
                foreach(Way way in town.Ways)
                {
                    List<Location> l = new List<Location>();
                    foreach (double refValue in way.RefValues)
                    {
                        BorderPoint p = BorderPoints[refValue];
                        l.Add( new Location(p.Lat, p.Lon));
                    }
                    allRoutes.Add(l);
                }

                locs.AddRange(allRoutes[0]);
                allRoutes.Remove(allRoutes[0]);

                double distance = 1000000;
                int count = 0;
                int linecount = 0;
                bool endpoint = true;
                while(allRoutes.Count >0)
                {
                    foreach (var route in allRoutes)
                    {
                        double newDistance = DistanceTo(locs[locs.Count - 1].Latitude, locs[locs.Count - 1].Longitude, route[0].Latitude, route[0].Longitude);
                        if (newDistance < distance)
                        {
                            distance = newDistance;
                            linecount = count;
                            endpoint = true;
                        }
                        newDistance = DistanceTo(locs[locs.Count - 1].Latitude, locs[locs.Count - 1].Longitude, route[route.Count - 1].Latitude, route[route.Count - 1].Longitude);
                        if (newDistance < distance)
                        {
                            distance = newDistance;
                            linecount = count;
                            endpoint = false;
                        }
                        count++;
                    }
                    if (endpoint)
                    {
                        locs.AddRange(allRoutes[linecount]);
                    }
                    else
                    {
                        for (int i = allRoutes[linecount].Count - 1; i != -1; i--)
                        {
                            locs.Add(allRoutes[linecount][i]);
                        }
                    }
                    allRoutes.RemoveAt(linecount);
                    count = 0;
                    distance = 1000000;
                }

                return locs;
            }
            else
            {
                return null;
            }
        }

        private static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }
    }
}
