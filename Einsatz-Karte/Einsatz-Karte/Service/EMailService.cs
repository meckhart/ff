﻿using Einsatz_Karte.Model;
using Limilabs.Client.IMAP;
using Limilabs.Client.SMTP;
using Limilabs.Mail;
using Limilabs.Mail.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Einsatz_Karte.Service
{
    public static class EMailService
    {
        static string _ImapAddress;
        static string _SmtpAddress;
        static string _EMailAddress;
        static string _EMailPassword;

        static EMailService()
        {
            _ImapAddress = ConfigService.ImapAddress;
            _SmtpAddress = ConfigService.SmtpAddress;
            _EMailAddress = ConfigService.EmailAddress;
            _EMailPassword = ConfigService.EmailPassword;
        }

        public static async Task<AlertInfo> CheckIfAlert()
        {
            using (Imap imap = new Imap())
            {
                await imap.ConnectSSL(_ImapAddress, 993);  // or ConnectSSL for SSL
                await imap.LoginAsync(_EMailAddress, _EMailPassword);

                await imap.SelectInboxAsync();
                List<long> uids = await imap.SearchAsync(Flag.All);
                long uid = uids[uids.Count-1];
                IMail email = new MailBuilder().CreateFromEml(await imap.GetMessageByUIDAsync(uid));
                
                string subject = email.Subject;
                int attachmentCount = email.Attachments.Count;
                if (attachmentCount > 0)
                    await FileService.SavePDF(email.Attachments[0]);
                await imap.CloseAsync();
                String [] s = await FileService.GetPdfAlertLocation();
                if (email.Date != null)
                {
                    return new AlertInfo(null, s[0], s[1], email.Date.Value);
                }
                else
                {
                    return new AlertInfo(null, s[0], s[1], DateTime.Now);
                }

                
            }
        }

        public static async void SendMailForReports()
        {
            StorageFile attachmentFile=null;
            if (GlobalVariablesService.AlertPdfPath != "" && GlobalVariablesService.AlertPdfPath!=null)
            {
                attachmentFile = await Windows.Storage.StorageFile.GetFileFromPathAsync(GlobalVariablesService.AlertPdfPath);
            }

            string receipientSMTP = _SmtpAddress;

            using (Smtp client = new Smtp())
            {
                byte[] b = new byte[3];
                await client.Connect(_SmtpAddress, 25);
                await client.UseBestLoginAsync(_EMailAddress, _EMailPassword);
                IMail email;
                String receiver = ConfigService.EmailsForReport;
                if(receiver==null)
                {
                    return;
                }
                foreach (string newreceiver in receiver.Split(';'))
                {
                    if (attachmentFile != null)
                    {
                        email = Limilabs.Mail.Fluent.Mail.Text("Hello")
                            .From(_EMailAddress)
                            .To(newreceiver)
                            .Html(await ReportService.GetHtmlForReportPage1() + await ReportService.GetHtmlForReportPage2())
                            .AddAttachment(await ReadFile(attachmentFile))
                            .SetFileName("EinsatzFax.pdf")
                            .Create();
                    }
                    else
                    {
                        email = Limilabs.Mail.Fluent.Mail.Text("Hello")
                            .From(_EMailAddress)
                            .To(newreceiver)
                            .Html(await ReportService.GetHtmlForReportPage1() + await ReportService.GetHtmlForReportPage2())
                            .Create();
                    }
                    client.SendMessage(email);
                }
                
                
                client.Close();
            } 

        }

        private static async Task<byte[]> ReadFile(StorageFile file)
        {
            byte[] fileBytes = null;
            using (IRandomAccessStreamWithContentType stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (DataReader reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }

            return fileBytes;
        }
    }
}
