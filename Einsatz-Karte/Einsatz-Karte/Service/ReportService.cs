﻿using Einsatz_Karte.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Einsatz_Karte.Service
{
    public static class ReportService
    {
        public static IEnumerable<Firefighter> Team;
        public static IEnumerable<Vehicle> Vehicles;
        public static Report AlertReport;
        public static int DefaultKm;

        static ReportService()
        {
            Team = Enumerable.Empty<Firefighter>();
            Vehicles = Enumerable.Empty<Vehicle>();
            AlertReport = new Report();
        }

        public static async Task<string> GetHtmlAlertReport()
        {
            string html = "";
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Report-TemplateNew.html");
            html = await FileIO.ReadTextAsync(sf);
            return html;
        }

        public static async Task<string> GetHtmlForReportPage1()
        {

            string html = "";
            StorageFile sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Report-Template.html");
            html = await FileIO.ReadTextAsync(sf);
            string bool1, bool2, bool3, bool4;

            bool1 = BoolToString(AlertReport.IsFireAlert);
            bool2 = BoolToString(AlertReport.IsFalseAlert);
            bool3 = BoolToString(AlertReport.IsTechniqueAlert);
            bool4 = BoolToString(AlertReport.IsOtherAlert);
            string info = "<br/>";
            if(AlertReport.Info!=null)
            {
                info+=AlertReport.Info.Replace("\n","<br/>");
            }

            html = String.Format(html,
                bool1, //0
                bool2,//1
                bool3,//2
                bool4,//3
                AlertReport.Date.Day + ":" + AlertReport.Date.Month + ":" + AlertReport.Date.Year,//4
                String.Format("{0,-2}", AlertReport.AlertTime.Hour) + ":" + String.Format("{0,-2}", AlertReport.AlertTime.Minute),//5
                String.Format("{0,-2}", AlertReport.EndTime.Hour) + ":" + String.Format("{0,-2}", AlertReport.EndTime.Minute),//6
                AlertReport.UseStreet,//7
                AlertReport.MissionControlBrigade,//8
                AlertReport.Owner,//9
                AlertReport.Vulgo,//10
                AlertReport.PLZ,//11
                AlertReport.OwnerLocation,//12
                AlertReport.OwnerStreet,//13
                AlertReport.LicensePlate,//14
                AlertReport.OtherAlert,//15
                AlertReport.UseLocation,//16
                info//17
                );

            int count = 0;
            html += "<table class=\"tg2\" style=\"undefined;table-layout: fixed; width: 100%\">";
            html += "<colgroup>";
            html +="<col style=\"width: 390px\">";
            html +="<col style=\"width: 390px\">";
            html += "</colgroup>";
            html += "<tr> <th class=\"tg-031e\" colspan=\"2\">EINGESETZTE FAHRZEUGE<br></th></tr>";

            if(Vehicles.Count()==0)
            {
                Vehicles = await FileService.GetVehiclesXML();
            }

            foreach (Vehicle vh in Vehicles)
            {
                if (count % 2 == 0)
                    html += "<tr>";
                html += "<td class=\"tg-031e\"> <input type=\"checkbox\" disabled=\"disabled\"";
                if (vh.IsTapped)
                {
                    html += "checked=\"checked\"";
                }
                html += "\"/> <label>" + String.Format("{0,-5}", vh.KM) + " km " + vh.Name;
                if(vh.Driver!=null)
                {
                    html += "        18 Fahrer: " + vh.Driver.Lastname + " " + vh.Driver.Firstname + "</label></td>";
                }
                else
                {
                    html += "</label></td>";
                }
                if (count % 2 == 1)
                    html += "</tr>";
                count++;
            }
            html += "</table>";

            sf = await ApplicationData.Current.LocalFolder.GetFileAsync("Style.html");
            html = await FileIO.ReadTextAsync(sf) + html;
            return html;
        }

        public static async Task<string> GetHtmlForReportPage2()
        {
            if(Team.Count()==0)
            {
                Team = await FileService.GetFirefighterXML();
            }
            int count = 0;
            string htmlContent;
           // htmlContent = "<!DOCTYPE html><html><head></head><body>";
            htmlContent = "<style> table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 5px;} input[type=checkbox] {width:10; height:10;}</style>";

            htmlContent += "<label>Mannschaft:</label>";
            htmlContent += "<table style=\"width:100%\">";
            foreach (Firefighter ff in Team)
            {
                if (count % 2 == 0)
                    htmlContent += "<tr>";
                htmlContent += "<td> <input type=\"checkbox\" disabled=\"disabled\"";
                if (ff.IsTapped)
                {
                    htmlContent += "checked=\"checked\"";
                }
                htmlContent += "\"/> <label>" + String.Format("{0,-5}", ff.Rank) + " " + ff.Lastname + " " + ff.Firstname + "</label></td>";
                if (count % 2 == 1)
                    htmlContent += "</tr>";
                count++;
            }
            htmlContent += "</table>";


            if (Vehicles.Count() == 0)
            {
                Vehicles = await FileService.GetVehiclesXML();
            }
            count = 0;
            htmlContent += "<label></label>";
            //htmlContent += "<label>Fahrzeuge:</label>";
            //htmlContent += "<table style=\"width:100%\">";
            //foreach (Vehicle vh in Vehicles)
            //{
            //    if (count % 2 == 0)
            //        htmlContent += "<tr>";
            //    htmlContent += "<td> <input type=\"checkbox\" disabled=\"disabled\"";
            //    if (vh.IsTapped)
            //    {
            //        htmlContent += "checked=\"checked\"";
            //    }
            //    htmlContent += "\"/> <label>" + String.Format("{0,-5}", vh.KM) + " km "+  vh.Name+"</label></td>";
            //    if (count % 2 == 1)
            //        htmlContent += "</tr>";
            //    count++;
            //}
            //htmlContent += "</table>";

            //htmlContent += "</body></html>";

            return htmlContent;
        }

        private static String BoolToString(bool b)
        {
            if (b)
                return "X";
            else
                return "O";
        }
    }
}
