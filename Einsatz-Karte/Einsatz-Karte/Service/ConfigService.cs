﻿using Einsatz_Karte.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Service
{
    public static class ConfigService
    {
        public static Dictionary<string, string> Config;

        public static string BingMapsKey;
        public static string GoogleMapsKey;
        public static string EmailAddress;
        public static string EmailPassword;
        public static string EmailsForReport;
        public static bool GPS;
        public static string Home;
        public static string ImapAddress;
        public static string SmtpAddress;
     
        static ConfigService()
        {
            
        }

        public static async void LoadConfig()
        {
            Config = new Dictionary<string, string>();
            var items = await FileService.GetConfigXML();
            foreach (ConfigItem item in items)
                Config.Add(item.ConfigName, item.Value);
            SetConfigToFields();
        }

        public static async void SaveConfig(IEnumerable<ConfigItem> items)
        {
            bool ready = await FileService.SaveConfigXML(items);
            if (ready)
            {
                LoadConfig();
            }
        }

        private static void SetConfigToFields()
        {
            if (Config.Keys.Contains("BingMapsKey"))
                BingMapsKey = Config["BingMapsKey"];
            if (Config.Keys.Contains("GoogleMapsKey"))
                GoogleMapsKey = Config["GoogleMapsKey"];
            if (Config.Keys.Contains("EmailAddress"))
                EmailAddress = Config["EmailAddress"];
            if (Config.Keys.Contains("EmailPassword"))
                EmailPassword = Config["EmailPassword"];
            if (Config.Keys.Contains("GPS"))
                GPS = Convert.ToBoolean(Config["GPS"]);
            else
                GPS = false;
            if (Config.Keys.Contains("Home"))
                Home = Config["Home"];
            if (Config.Keys.Contains("ImapAddress"))
                ImapAddress = Config["ImapAddress"];
            if (Config.Keys.Contains("SmtpAddress"))
             SmtpAddress = Config["SmtpAddress"];
        }
    }
}
