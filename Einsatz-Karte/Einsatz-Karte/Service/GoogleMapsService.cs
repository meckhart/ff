﻿using Bing.Maps;
using Einsatz_Karte.Model.APIXML;
using Einsatz_Karte.Model.CustomExecptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Windows.Data.Xml.Dom;

namespace Einsatz_Karte.Service
{
    public static class GoogleMapsService
    {
        public static async Task<Location> GetGeoCodeFromAddress(string address)
        {
            if (string.IsNullOrEmpty(ConfigService.GoogleMapsKey))
            {
                throw new RestException(RestExceptionType.NoKeyInConfig, "Es wurde kein Google Maps Key in der Config Datei gefunden!");
            }
            String addressRequest = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key={1}", address, ConfigService.GoogleMapsKey);
            XmlDocument geocodeResponse = await GetXmlResponse(addressRequest);
            if (geocodeResponse == null)
            {
                throw new RestException(RestExceptionType.NoConnection, "Der Google Maps Dienst konnte nicht erreicht werden!");
            }

            XmlSerializer ser = new XmlSerializer(typeof(GeocodeResponse));
            GeocodeResponse response;
            using (Stream s = GenerateStreamFromString(geocodeResponse.GetXml()))
            {
                using (XmlReader reader = XmlReader.Create(s))
                {
                    response = (GeocodeResponse)ser.Deserialize(reader);
                }
            }

            if (response.Status == "OK")
            {
                Location l = new Location();
                l.Latitude = response.Result.Geometry.Location.Lat;
                l.Longitude = response.Result.Geometry.Location.Lng;
                return l;
            }
            else if (response.Status == "ZERO_RESULTS")
            {
                throw new RestException(RestExceptionType.NoLocationFound, "Die Adresse konnte nicht gefunden werden!");
            }
            else if (response.Status == "OVER_QUERY_LIMIT")
            {
                throw new RestException(RestExceptionType.OverQqueryLimit, "Die täglichen Anfragen an den Google Maps Server wurden überschritten!");
            }
            else 
            {
                throw new RestException(RestExceptionType.Common, "Ein allgemeiner Fehler ist aufgetaucht! ("+response.Status+" : "+response.ErrorMessage+")");
            }
        }

        private static async Task<XmlDocument> GetXmlResponse(string requestUrl)
        {
            try
            {
                HttpClient myClient = new HttpClient();
                HttpResponseMessage msg = await myClient.GetAsync(requestUrl);
                XmlDocument doc = new XmlDocument();
                string s = await msg.Content.ReadAsStringAsync();
                doc.LoadXml(await msg.Content.ReadAsStringAsync());
                return doc;
            }
            catch
            {
                return null;
            }
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
