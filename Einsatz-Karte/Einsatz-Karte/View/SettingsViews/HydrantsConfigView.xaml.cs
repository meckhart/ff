﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Einsatz_Karte.Common;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Einsatz_Karte.ViewModel.Settings;
using Einsatz_Karte.Model;
using Bing.Maps;
using Windows.UI.Popups;

// Die Elementvorlage für die Seite "Elementdetails" ist unter http://go.microsoft.com/fwlink/?LinkId=234232 dokumentiert.

namespace Einsatz_Karte.View.SettingsViews
{
    /// <summary>
    /// Eine Seite, auf der Details für ein einzelnen Element innerhalb einer Gruppe angezeigt werden, während Gesten zugelassen werden, um
    /// durch andere Elemente derselben Gruppe zu blättern.
    /// </summary>
    public sealed partial class HydrantsConfigView : Page
    {
        public HydrantsConfigView()
        {
            this.InitializeComponent();
            viewModel.LocationChanged+=OnLocationChanged;
            SetMapViewOnLocation();
        }

        private void OnLocationChanged(object sender, EventArgs e)
        {
            SetMapViewOnLocation();
        }


        private void SetMapViewOnLocation()
        {
            HydrantsConfigViewModel mvm = (HydrantsConfigViewModel)this.Resources["viewModel"];
            if (mvm.SelectedItem != null)
            {
                Hydrant h = (Hydrant)mvm.SelectedItem;
                if (h.Latitude > 0 && h.Longitude > 0)
                {
                    MyMap.SetZoomLevel(16f, new TimeSpan(0));
                    MyMap.SetView(new Location(h.Latitude, h.Longitude));
                }
            }
        }

        private void ItemPopUp_Opened(object sender, object e)
        {
            SetMapViewOnLocation();
        }
        private void GoBack(object sender, TappedRoutedEventArgs e)
        {
            if (viewModel.ItemsChanged)
            {
                MessageDialog md = new MessageDialog("Daten wurden noch nicht gespeichert. Trotzdem schließen? (Änderungen gehen verloren)");
                UICommand uicYes = new UICommand("Ja");
                uicYes.Invoked = YesBtnClick;
                md.Commands.Add(uicYes);

                UICommand uicNo = new UICommand("Nein");
                md.Commands.Add(uicNo);

                md.ShowAsync();
            }
            else
            {
                this.Frame.GoBack();
            }
        }

        private void YesBtnClick(IUICommand command)
        {
            this.Frame.GoBack();
        }

    }
}
