﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Einsatz_Karte.Common;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Printing;
using Windows.Graphics.Printing;
using Einsatz_Karte.Service;

// Die Elementvorlage für die Seite "Elementdetails" ist unter http://go.microsoft.com/fwlink/?LinkId=234232 dokumentiert.

namespace Einsatz_Karte.View
{
    //public delegate void SetHTMLOnViewHandler(string s);
    /// <summary>
    /// Eine Seite, auf der Details für ein einzelnen Element innerhalb einer Gruppe angezeigt werden, während Gesten zugelassen werden, um
    /// durch andere Elemente derselben Gruppe zu blättern.
    /// </summary>
    public sealed partial class ReportView : BasePrintPage
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private ViewModel.ReportViewModel mvm;

        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ReportView()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;

            mvm = (ViewModel.ReportViewModel)this.Resources["reportViewModel"];
            //mvm.SetHTMLOnView1+=OnSetHTMLOnView1;
            //mvm.SetHTMLOnView2+=OnSetHTMLOnView2;
            //webView1.NavigateToString(mvm.HtmlContent1);
            //webView2.NavigateToString(mvm.HtmlContent2);
            SetData();
     
        }

        private async void SetData()
        {
            string html1=await Einsatz_Karte.Service.ReportService.GetHtmlForReportPage1();
            string html2 = await Einsatz_Karte.Service.ReportService.GetHtmlForReportPage2();
            webView1.NavigateToString(html1);
            webView2.NavigateToString(html2);
            PreparePrintContent();
            WebView wv1 = new WebView();
            wv1.NavigateToString(html1);
            wv1.Height = 2000;
            wv1.Width = 785;
            WebView wv2 = new WebView();
            wv2.NavigateToString(html2);
            wv2.Height = 2000;
            wv2.Width = 785;
            Einsatz_Karte.Service.PrintHelperService.webView1 = wv1;
            Einsatz_Karte.Service.PrintHelperService.webView2 = wv2;
        }

        private async void Print()
        {
            await Windows.Graphics.Printing.PrintManager.ShowPrintUIAsync();
        }

        private void OnSetHTMLOnView2(string s)
        {
            
        }

        private void OnSetHTMLOnView1(string s)
        {
            webView1.NavigateToString(s);
        }

        protected override void PreparePrintContent()
        {
            MyWebView1 = webView1;
            MyWebView2 = webView2;
        }




        /// <summary>
        /// Füllt die Seite mit Inhalt auf, der bei der Navigation übergeben wird.  Gespeicherte Zustände werden ebenfalls
        /// bereitgestellt, wenn eine Seite aus einer vorherigen Sitzung neu erstellt wird.
        /// </summary>
        /// <param name="sender">
        /// Die Quelle des Ereignisses, normalerweise <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Ereignisdaten, die die Navigationsparameter bereitstellen, die an
        /// <see cref="Frame.Navigate(Type, Object)"/> als diese Seite ursprünglich angefordert wurde und
        /// ein Wörterbuch des Zustands, der von dieser Seite während einer früheren
        /// beibehalten wurde.  Der Zustand ist beim ersten Aufrufen einer Seite NULL.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            object navigationParameter;
            if (e.PageState != null && e.PageState.ContainsKey("SelectedItem"))
            {
                navigationParameter = e.PageState["SelectedItem"];
            }

            // TODO: this.DefaultViewModel["Group"] eine bindbare Gruppe zuweisen
            // TODO: this.DefaultViewModel["Items"] eine Auflistung von bindbaren Elementen zuweisen
            // TODO: this.flipView.SelectedItem das ausgewählte Element zuweisen
        }

        #region NavigationHelper-Registrierung

        /// Die in diesem Abschnitt bereitgestellten Methoden lassen sich einfach für Folgendes verwenden:
        /// Reaktion von NavigationHelper auf die Navigationsmethoden der Seite.
        /// 
        /// Platzieren von seitenspezifischer Logik in Ereignishandlern für  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// und <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// Der Navigationsparameter ist in der LoadState-Methode verfügbar 
        /// zusätzlich zum Seitenzustand, der während einer früheren Sitzung beibehalten wurde.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion#

        private async void AppBarButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            UnregisterForPrinting();
            RegisterForPrinting();
            await Windows.Graphics.Printing.PrintManager.ShowPrintUIAsync();
            EMailService.SendMailForReports();
            //UnregisterForPrinting();
            
        }

       
    }
}
