﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Einsatz_Karte.CustomControls;
using Windows.UI;
using Einsatz_Karte.Common;
using Einsatz_Karte.Model;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Einsatz_Karte.View
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>

    public delegate void SetRouteHandlerView(MapPolyline mp,  RouteType rt);
    public delegate void SetPolygonsHandler(IEnumerable<MapPolygon> mps);
    public delegate void SetPolylinesHandler(IEnumerable<MapPolyline> mps);
    public delegate void SetCircleHandler(IEnumerable<CustomMapCircle> mps);
    public delegate void SetRouteHandlerViewLocation(Location l);
    public delegate void RemoveRouteFromMapHandler(RouteType rt);

    public sealed partial class MapView : Page
    {
        private ViewModel.MapViewModel mvm;
        private Pushpin PinToMove;
        private Location Center;
        MapShapeLayer shapeLayer;
        MapShapeLayer shapeLayerAlert;
        MapShapeLayer shapeLayerHydrant;
        MapShapeLayer shapeLayerPolygons;
        MapShapeLayer shapeLayerPolylines;
        MapShapeLayer shapeLayerCircles;
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        //private MapShapeLayer routeLayer;
        
        public MapView()
        {
            this.InitializeComponent();
            mvm = (ViewModel.MapViewModel)this.Resources["mapViewModel"];
            mvm.IsLoading = true;
            mvm.SetRouteView += SetRoute;
            mvm.SetRouteViewLocation += SetMapViewOnLocation;
            mvm.RemoveRouteFromMap += RemoveRouteFromMap;
            mvm.SetPolygons += SetPolygons;
            mvm.SetPolylines += SetPolylines;
            mvm.SetCircles += SetCircle;
            mvm.RefreshMap += RefreshMap;
            mvm.SetTowns += SetTowns;
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Required;
            MyMap.ShapeLayers.Add(MSL_Towns);
        }

        private void RemoveRouteFromMap(RouteType rt)
        {
            if ((int)rt == (int)RouteType.Manuel)
            {
                if (shapeLayer != null)
                    MyMap.ShapeLayers.Remove(shapeLayer);
                shapeLayer = new MapShapeLayer();
            }
            if ((int)rt == (int)RouteType.Alert)
            {
                if (shapeLayerAlert != null)
                    MyMap.ShapeLayers.Remove(shapeLayerAlert);
                shapeLayerAlert = new MapShapeLayer();
            }
            if ((int)rt == (int)RouteType.Hydrant)
            {
                if (shapeLayerHydrant != null)
                    MyMap.ShapeLayers.Remove(shapeLayerHydrant);
                shapeLayerHydrant = new MapShapeLayer();
            }
        }

        private void SetRoute(MapPolyline mp, RouteType rt)
        {
            if ((int)rt == (int)RouteType.Manuel)
            {
                if (shapeLayer != null)
                    MyMap.ShapeLayers.Remove(shapeLayer);
                shapeLayer = new MapShapeLayer();

                MapPolyline polyline = new MapPolyline();
                polyline = mp;
                polyline.Color = Windows.UI.Colors.Red;
                polyline.Width = 3;

                shapeLayer.Shapes.Add(polyline);

                MyMap.ShapeLayers.Add(shapeLayer);
                MyMap.SetView(new LocationRect(polyline.Locations));
            }
            if((int)rt==(int) RouteType.Alert)
            {
                if (shapeLayerAlert != null)
                    MyMap.ShapeLayers.Remove(shapeLayerAlert);
                shapeLayerAlert = new MapShapeLayer();

                MapPolyline polyline = new MapPolyline();
                polyline = mp;
                polyline.Color = Windows.UI.Colors.Blue;
                polyline.Width = 6;

                shapeLayerAlert.Shapes.Add(polyline);

                MyMap.ShapeLayers.Add(shapeLayerAlert);
                MyMap.SetView(new LocationRect(polyline.Locations));
            }
            if((int)rt == (int) RouteType.Hydrant)
            {
                if (shapeLayerHydrant != null)
                    MyMap.ShapeLayers.Remove(shapeLayerHydrant);
                shapeLayerHydrant = new MapShapeLayer();

                MapPolyline polyline = new MapPolyline();
                polyline = mp;
                polyline.Color = Windows.UI.Colors.Green;
                polyline.Width = 4;

                shapeLayerHydrant.Shapes.Add(polyline);

                MyMap.ShapeLayers.Add(shapeLayerHydrant);
                MyMap.SetView(new LocationRect(polyline.Locations));
            }
        }

        private void SetPolygons(IEnumerable<MapPolygon> mps)
        {
            if (shapeLayerPolygons != null)
            {
                MyMap.ShapeLayers.Remove(shapeLayerPolygons);
            }
            shapeLayerPolygons = new MapShapeLayer();
            foreach(MapPolygon mp in mps)
            { 
                MapPolygon mpNew = new MapPolygon();
                mpNew.Locations = mp.Locations;
                mpNew.FillColor = mp.FillColor;
                shapeLayerPolygons.Shapes.Add(mpNew);
            }
            MyMap.ShapeLayers.Add(shapeLayerPolygons);
        }
        private void SetPolylines(IEnumerable<MapPolyline> mps)
        {
            if (shapeLayerPolylines != null)
            {
                MyMap.ShapeLayers.Remove(shapeLayerPolylines);
            }
            shapeLayerPolylines = new MapShapeLayer();
            foreach (MapPolyline mp in mps)
            {
                MapPolyline mpNew = new MapPolyline();
                mpNew.Locations = mp.Locations;
                mpNew.Color = mp.Color;
                mpNew.Width = mp.Width;
                shapeLayerPolylines.Shapes.Add(mpNew);
            }
            MyMap.ShapeLayers.Add(shapeLayerPolylines);

        }
        private void SetCircle(IEnumerable<CustomMapCircle> mps)
        {
            if (shapeLayerCircles != null)
            {
                MyMap.ShapeLayers.Remove(shapeLayerCircles);
            }
            shapeLayerCircles = new MapShapeLayer();
            foreach (CustomMapCircle mp in mps)
            {
                MapPolygon mpNew = new MapPolygon();
                mpNew.Locations = mp.Circle.Locations;
                mpNew.FillColor = mp.Circle.FillColor;
                shapeLayerCircles.Shapes.Add(mpNew);

                MapPolyline mpNew1 = new MapPolyline();
                mpNew1.Color = Colors.Black;
                mpNew1.Width = 3;
                mpNew1.Locations.Add(mp.Loc1.Locs);
                mpNew1.Locations.Add(mp.Loc2.Locs);
                shapeLayerCircles.Shapes.Add(mpNew1);
            }
            MyMap.ShapeLayers.Add(shapeLayerCircles);

        }

        private void SetMapViewOnLocation(Location l)
        {
            MyMap.SetZoomLevel(14f, new TimeSpan(0));
            MyMap.SetView(l);
            mvm.IsLoading = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HydrantPopUp.IsOpen = false;
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (shapeLayer != null)
                MyMap.ShapeLayers.Remove(shapeLayer);
            shapeLayer = new MapShapeLayer();
        }      

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (shapeLayerHydrant != null)
                MyMap.ShapeLayers.Remove(shapeLayerHydrant);
            shapeLayerHydrant = new MapShapeLayer();
        }

        private void Pushpin_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Pushpin p = (Pushpin)sender;
            mvm.SetTappedHydrant(p.Tag.ToString());
            HydrantPopUp.IsOpen = true;
            MyMap.ZoomLevel = MyMap.ZoomLevel;
        }

        private void Settlement_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Pushpin p = (Pushpin)sender;
            mvm.SetTappedSettlement(p.Tag.ToString());
            SettlementPopUp.IsOpen = true;
            MyMap.ZoomLevel = MyMap.ZoomLevel;
        }

        private void GoToSettlenment(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SettlementView), mvm.Container.TappedSettlement);
        }

        private void GoToSettlenments(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.SettlementsView));
        }

        private void GoToSettings(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.SettingsView));
        }
        
        private void GoToTeam(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.TeamView));
        }
        private void GoToVehicle(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.VehiclesView));
        }
        private void GoToReport(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.ReportView));
        }
        private void GoToAlertReport(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.AlertReportView));
        }
        private void GoToReportEdit(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Einsatz_Karte.View.ReportEditView));
        }
        private void PopupSettlementClose(object sender, TappedRoutedEventArgs e)
        {
            SettlementPopUp.IsOpen = false;
        }
        private void MapTapped(object sender, TappedRoutedEventArgs e)
        {
            Point p = e.GetPosition(MyMap);
            Location location = new Location();
            MyMap.TryPixelToLocation(p, out location);
            mvm.MapTapped(location);
        }
        private void RefreshMap(object sender, EventArgs e)
        {
            MyMap.SetView(MyMap.Bounds);
        }
        private void Pushpin_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            PinToMove = (Pushpin)sender;
            Center = MyMap.Center;   

        }
        private void MyMap_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (PinToMove != null)
            {
                String[] tag = PinToMove.Tag.ToString().Split(':');
                int count1 = Convert.ToInt32(tag[1]);
                int count2 = Convert.ToInt32(tag[2]);

                var pointer = e.GetCurrentPoint(MyMap);
                Point newp = new Point(pointer.RawPosition.X, pointer.RawPosition.Y);
                Location location = new Location();
                MyMap.TryPixelToLocation(newp, out location);

                if (tag[0].Equals("A"))
                {
                    mvm.SetArePinNew(count1, count2, location);
                }
                else if(tag[0].Equals("L"))
                {
                    mvm.SetLinePinNew(count1, count2, location); 
                }
                else if (tag[0].Equals("C"))
                {
                    mvm.SetCirclePinNew(count1, count2, location);                   
                }
            }
        }
        private void Pushpin_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            PinToMove = null;
        }

        private void MyMap_ViewChanged(object sender, ViewChangedEventArgs e)
        {
            if(PinToMove!=null)
            {
                MyMap.Center = Center;
            }
        }

        private void MyMap_PointerReleasedOverride(object sender, PointerRoutedEventArgs e)
        {
            PinToMove = null;
        }

        private void MyMap_Loaded(object sender, RoutedEventArgs e)
        {
            HydrantPopUpGrid.MinWidth = MyMap.ActualWidth - 100;
            HydrantPopUpGrid.MinHeight = MyMap.ActualHeight - 100;
            HydrantPopUpGrid.Width = MyMap.ActualWidth - 100;
            HydrantPopUpGrid.Height = MyMap.ActualHeight - 100;
            HydrantPopUpGrid.MaxWidth = MyMap.ActualWidth - 100;
            HydrantPopUpGrid.MaxHeight = MyMap.ActualHeight - 100;
            HydrantPopUpImage.MaxWidth = HydrantPopUpGrid.Width / 2;

            SettlementPopUpGrid.MinWidth = MyMap.ActualWidth - 100;
            SettlementPopUpGrid.MinHeight = MyMap.ActualHeight - 100;
            SettlementPopUpGrid.Width = MyMap.ActualWidth - 100;
            SettlementPopUpGrid.Height = MyMap.ActualHeight - 100;
            SettlementPopUpGrid.MaxWidth = MyMap.ActualWidth - 100;
            SettlementPopUpGrid.MaxHeight = MyMap.ActualHeight - 100;
            SettlementPopUpImage.MaxWidth = HydrantPopUpGrid.Width / 2;

        }

        private void SetTowns(IEnumerable<MapPolyline> mps)
        {
            foreach (MapPolyline mp in mps)
            {
                MapPolyline mpNew = new MapPolyline();
                mpNew.Locations = mp.Locations;
                mpNew.Color = mp.Color;
                mpNew.Width = mp.Width;
                MSL_Towns.Shapes.Add(mpNew);
            }
        }

        //private async void test()
        //{

        //    MapShapeLayer Borders = new MapShapeLayer();
        //    List<List<Location>> Cities = new List<List<Location>>();

        //    Windows.Storage.StorageFile storageFile = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync("Greznen.xml");
        //    Windows.Data.Xml.Dom.XmlDocument doc = new Windows.Data.Xml.Dom.XmlDocument();
        //    string xml = await Windows.Storage.FileIO.ReadTextAsync(storageFile, Windows.Storage.Streams.UnicodeEncoding.Utf8);
        //    doc.LoadXml(xml);

        //    var help = doc.GetElementsByTagName("node");
        //    Dictionary<double,Location> locs = new  Dictionary<double,Location>();
        //    foreach (var a in help)
        //    {
        //        double id=Convert.ToDouble(a.Attributes[0].NodeValue);
        //        double lat = Convert.ToDouble(a.Attributes[1].NodeValue);
        //        double lon = Convert.ToDouble(a.Attributes[2].NodeValue);
        //        locs.Add(id, new Location(lat, lon));
        //    }

        //    var ways = doc.GetElementsByTagName("way");
        //    var cities = doc.GetElementsByTagName("relation");

        //    foreach (var city in cities)
        //    {
        //        List<string> refWays = new List<string>();
        //        List<List<Location>> newlocs = new List<List<Location>>();

        //        foreach (var child in city.ChildNodes)
        //        {
        //            if (child.Attributes != null && child.Attributes.Count > 0 && child.Attributes[0].NodeName == "type" && child.Attributes[0].NodeValue.ToString()=="way")
        //            {
        //                refWays.Add(child.Attributes[1].NodeValue.ToString());
        //            }
        //        }

        //        foreach()

        //        break;
        //    }

            //var ways = doc.GetElementsByTagName("way");

            //int count = 0;

            //foreach (var way in ways)
            //{
            //    count++;
            //    MapPolyline line = new MapPolyline();
            //    Cities.Add(new List<Location>());
            //    foreach (var child in way.ChildNodes)
            //    {
            //        if (child.Attributes != null && child.Attributes.Count>0 && child.Attributes[0].NodeName=="ref")
            //        {
            //            double id = Convert.ToDouble(child.Attributes[0].NodeValue);
            //            line.Locations.Add(locs[id]);
            //            Cities[Cities.Count - 1].Add(locs[id]);
            //        }
            //    }
            //    Borders.Shapes.Add(line);
            //    if (count == 10)
            //    {
            //        break;
            //    }
            //}
            //MyMap.ShapeLayers.Add(Borders);



            //var help = doc.GetElementsByTagName("node");
            //MapPolyline p = new MapPolyline();
            //foreach (var a in help)
            //{
            //    double lat=Convert.ToDouble( a.Attributes[1].NodeValue);
            //    double lon = Convert.ToDouble(a.Attributes[2].NodeValue);
            //    p.Locations.Add(new Location(lat, lon));
            //}
            //p.Color = Colors.Red;
            //p.Width = 10;
            //MapShapeLayer shapeLayer123=new MapShapeLayer();
            //shapeLayer123.Shapes.Add(p);

            //MyMap.ShapeLayers.Add(shapeLayer123);
        //}
    }
}
