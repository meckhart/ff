﻿using Einsatz_Karte.Common;
using Einsatz_Karte.View.SettingsViews;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Einsatz_Karte.View
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class SettingsView : Page
    {

        public SettingsView()
        {
            this.InitializeComponent();
        }

        private void GoBack(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void GoToConfig(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ConfigView));
        }

        private void GoToHydrants(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(HydrantsConfigView));
        }

        private void GoToSettlements(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SettlementsConfigView));
        }


    }
}
