﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Einsatz_Karte.Common;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage für die Seite "Elementdetails" ist unter http://go.microsoft.com/fwlink/?LinkId=234232 dokumentiert.

namespace Einsatz_Karte.View
{
    /// <summary>
    /// Eine Seite, auf der Details für ein einzelnen Element innerhalb einer Gruppe angezeigt werden, während Gesten zugelassen werden, um
    /// durch andere Elemente derselben Gruppe zu blättern.
    /// </summary>
    public sealed partial class ReportEditView : LayoutAwarePage
    {
        private NavigationHelper navigationHelper;
        int pointerId = -1;
        Windows.UI.Input.Inking.InkManager inkManager = null;
        Windows.UI.Input.Inking.InkDrawingAttributes drawingAttributes = null;
        Windows.UI.Input.Inking.InkDrawingAttributes lassoAttributes = null;
        Einsatz_Karte.Common.XamlInkRenderer renderer = null;
        private Einsatz_Karte.ViewModel.ReportEditViewModel mvm;
        /// <summary>
        /// NavigationHelper wird auf jeder Seite zur Unterstützung bei der Navigation verwendet und 
        /// Verwaltung der Prozesslebensdauer
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ReportEditView()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Required;

            mvm = (ViewModel.ReportEditViewModel)this.Resources["reporteditViewModel"];
            mvm.ClearCan+=OnClearCan;

            drawingAttributes = new Windows.UI.Input.Inking.InkDrawingAttributes();
            drawingAttributes.Color = Windows.UI.Colors.Red;
            double penSize = 2 + 2;
            drawingAttributes.Size = new Windows.Foundation.Size(penSize, penSize);
            drawingAttributes.IgnorePressure = true;
            drawingAttributes.FitToCurve = true;

            // Initialize lasso attributes. These are used in selection mode.
            lassoAttributes = new Windows.UI.Input.Inking.InkDrawingAttributes();
            lassoAttributes.Color = Windows.UI.Colors.Goldenrod;
            lassoAttributes.PenTip = Windows.UI.Input.Inking.PenTipShape.Circle;
            lassoAttributes.Size = new Windows.Foundation.Size(0.5f, 0.5f);

            // Create the InkManager and set the drawing attributes
            inkManager = new Windows.UI.Input.Inking.InkManager();
            inkManager.SetDefaultDrawingAttributes(drawingAttributes);
            inkManager.SetDefaultRecognizer(inkManager.GetRecognizers().First(p=>p.Name.Equals("Microsoft-Handschrifterkennung - Deutsch")));


            renderer = new Einsatz_Karte.Common.XamlInkRenderer(InkingArea);
        }

        private void OnClearCan(object sender, EventArgs e)
        {
            pointerId = -1;

            if (inkManager.AnySelected())
            {
                inkManager.DeleteSelected();

                // We don't know what has been erased so we clear the render
                // and add back all the ink saved in the ink manager
                renderer.Clear();
                renderer.AddInk(inkManager.GetStrokes());

                // There is no selection - disable movement
                AnchorSelection();
            }
            else
            {
                inkManager.DeleteAll();
                renderer.Clear();
            }
        }

        /// <summary>
        /// Füllt die Seite mit Inhalt auf, der bei der Navigation übergeben wird.  Gespeicherte Zustände werden ebenfalls
        /// bereitgestellt, wenn eine Seite aus einer vorherigen Sitzung neu erstellt wird.
        /// </summary>
        /// <param name="sender">
        /// Die Quelle des Ereignisses, normalerweise <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Ereignisdaten, die die Navigationsparameter bereitstellen, die an
        /// <see cref="Frame.Navigate(Type, Object)"/> als diese Seite ursprünglich angefordert wurde und
        /// ein Wörterbuch des Zustands, der von dieser Seite während einer früheren
        /// beibehalten wurde.  Der Zustand ist beim ersten Aufrufen einer Seite NULL.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            object navigationParameter;
            if (e.PageState != null && e.PageState.ContainsKey("SelectedItem"))
            {
                navigationParameter = e.PageState["SelectedItem"];
            }

            // TODO: this.DefaultViewModel["Group"] eine bindbare Gruppe zuweisen
            // TODO: this.DefaultViewModel["Items"] eine Auflistung von bindbaren Elementen zuweisen
            // TODO: this.flipView.SelectedItem das ausgewählte Element zuweisen
        }



        void AnchorSelection()
        {
            // Resize SelecionRect and remove and disable manipulation
            SelectionRect.Height = 0;
            SelectionRect.Width = 0;
            SelectionRect.ManipulationMode = Windows.UI.Xaml.Input.ManipulationModes.None;
        }

        void InkingArea_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            // Make sure no pointer is already inking (we allow only one 'active' pointer at a time)
            if (pointerId == -1)
            {
                var pointerPoint = e.GetCurrentPoint(InkingArea);


                // Determine ink manipulation mode
                switch (Helpers.GetPointerEventType(e))
                {
                    case Helpers.PointerEventType.Erase:
                        inkManager.Mode = Windows.UI.Input.Inking.InkManipulationMode.Erasing;
                        break;
                    case Helpers.PointerEventType.Ink:
                        inkManager.Mode = Windows.UI.Input.Inking.InkManipulationMode.Inking;
                        renderer.EnterLiveRendering(pointerPoint, drawingAttributes);
                        break;
                    case Helpers.PointerEventType.Select:
                        inkManager.Mode = Windows.UI.Input.Inking.InkManipulationMode.Selecting;
                        renderer.EnterLiveRendering(pointerPoint, lassoAttributes);
                        break;
                    default:
                        return; // pointer is neither inking nor erasing nor selecting: do nothing
                }

                // Clear selection
                inkManager.ClearSelection();
                renderer.UpdateSelection();
                AnchorSelection();

                inkManager.ProcessPointerDown(pointerPoint);

                pointerId = (int)pointerPoint.PointerId; // save pointer id so that no other pointer can ink until this one is released
            }
        }
        void InkingArea_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var pointerPoint = e.GetCurrentPoint(InkingArea);

            if (pointerId == (int)pointerPoint.PointerId)
            {
                switch (inkManager.Mode)
                {
                    case Windows.UI.Input.Inking.InkManipulationMode.Erasing:
                        // Check if something has been erased.
                        // In erase mode InkManager::ProcessPointerUpdate returns an invalidate
                        // rectangle: if it is not degenerate something has been erased
                        // In erase mode we don't bother processing intermediate points
                        var invalidateRect = (Windows.Foundation.Rect)inkManager.ProcessPointerUpdate(e.GetCurrentPoint(InkingArea));
                        if (invalidateRect.Height != 0 && invalidateRect.Width != 0)
                        {
                            // We don't know what has been erased so we clear the render
                            // and add back all the ink saved in the ink manager
                            renderer.Clear();
                            renderer.AddInk(inkManager.GetStrokes());
                        }
                        break;
                    case Windows.UI.Input.Inking.InkManipulationMode.Inking:
                    case Windows.UI.Input.Inking.InkManipulationMode.Selecting:
                        // Process intermediate points
                        var intermediatePoints = e.GetIntermediatePoints(InkingArea);
                        for (int i = intermediatePoints.Count - 1; i >= 0; i--)
                        {
                            inkManager.ProcessPointerUpdate(intermediatePoints[i]);
                        }

                        // Live rendering
                        renderer.UpdateLiveRender(pointerPoint);
                        break;
                }
            }
        }

        void InkingArea_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var pointerPoint = e.GetCurrentPoint(InkingArea);

            if (pointerId == (int)pointerPoint.PointerId)
            {
                pointerId = -1; // reset pointerId so that other pointers may enter live rendering mode

                var rect = inkManager.ProcessPointerUp(pointerPoint);
                switch (inkManager.Mode)
                {
                    case Windows.UI.Input.Inking.InkManipulationMode.Inking:
                        renderer.ExitLiveRendering(pointerPoint);
                        renderer.AddInk(inkManager.GetStrokes()[inkManager.GetStrokes().Count - 1]); // Add last stroke that was created to the Bezier render
                        break;

                    case Windows.UI.Input.Inking.InkManipulationMode.Selecting:
                        renderer.ExitLiveRendering(pointerPoint);

                        if (inkManager.AnySelected())
                        {
                            // Something has been selected

                            // Notify the renderer to update the selection
                            renderer.UpdateSelection();

                            // Resize SelectionRect and register event handlers to move the selection
                            Windows.UI.Xaml.Controls.Canvas.SetLeft(SelectionRect, rect.Left);
                            Windows.UI.Xaml.Controls.Canvas.SetTop(SelectionRect, rect.Top);
                            SelectionRect.Width = rect.Width;
                            SelectionRect.Height = rect.Height;
                            SelectionRect.ManipulationMode = Windows.UI.Xaml.Input.ManipulationModes.TranslateX | Windows.UI.Xaml.Input.ManipulationModes.TranslateY;
                        }
                        break;
                }
                WriteWrittenToTextBox();

            }
        }

        void InkingArea_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            // More complicated logics can be created, but for this simple example we pretend that
            // the pointer is released when it leaves the inking area
            InkingArea_PointerReleased(sender, e);
        }



        void SelectionRect_ManipulationDelta(object sender, Windows.UI.Xaml.Input.ManipulationDeltaRoutedEventArgs e)
        {
            // Get current position of selection's bounding box
            var curPos = new Windows.Foundation.Point(
                Windows.UI.Xaml.Controls.Canvas.GetLeft(SelectionRect),
                Windows.UI.Xaml.Controls.Canvas.GetTop(SelectionRect)
            );

            // Compute new position, making sure that the bounding box does not go outside the InkingArea
            var newPos = new Windows.Foundation.Point(
                Math.Max(0, Math.Min(curPos.X + e.Delta.Translation.X, InkingArea.ActualWidth - SelectionRect.Width)),
                Math.Max(0, Math.Min(curPos.Y + e.Delta.Translation.Y, InkingArea.ActualHeight - SelectionRect.Height))
            );

            // Compute the actual translation to pass to InkManager.MoveSelected
            var translation = new Windows.Foundation.Point(
                newPos.X - curPos.X,
                newPos.Y - curPos.Y
            );

            if (Math.Abs(translation.X) > 0 || Math.Abs(translation.Y) > 0)
            {
                // Move the selection's bounding box
                Windows.UI.Xaml.Controls.Canvas.SetLeft(SelectionRect, newPos.X);
                Windows.UI.Xaml.Controls.Canvas.SetTop(SelectionRect, newPos.Y);

                // Move selected ink
                inkManager.MoveSelected(translation);

                // Re-render everything
                renderer.Clear();
                renderer.AddInk(inkManager.GetStrokes());
                renderer.UpdateSelection();
            }

            e.Handled = true;
        }

        private async void WriteWrittenToTextBox()
        {
            if (inkManager.GetStrokes().Count > 0)
            {
                // the following call to RecognizeAsync may fail for various reasons, most notably if another recognition is in progress
                try
                {
                    var recognitionResults = await inkManager.RecognizeAsync(inkManager.AnySelected() ? Windows.UI.Input.Inking.InkRecognitionTarget.Selected : Windows.UI.Input.Inking.InkRecognitionTarget.All);

                    // Save recognition results to inkManager
                    inkManager.UpdateRecognitionResults(recognitionResults);

                    // Display recognition result
                    String str = "";
                    foreach (var r in recognitionResults)
                    {
                        str += " " + r.GetTextCandidates()[0];
                    }
                    mvm.Preview = str;
                }
                catch (System.Exception se)
                {
                }
            }
            else
            {

            }
        }

        #region NavigationHelper-Registrierung

        /// Die in diesem Abschnitt bereitgestellten Methoden lassen sich einfach für Folgendes verwenden:
        /// Reaktion von NavigationHelper auf die Navigationsmethoden der Seite.
        /// 
        /// Platzieren von seitenspezifischer Logik in Ereignishandlern für  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// und <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// Der Navigationsparameter ist in der LoadState-Methode verfügbar 
        /// zusätzlich zum Seitenzustand, der während einer früheren Sitzung beibehalten wurde.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            string field = tb.Tag.ToString();

            mvm.SetPreviewToTextBox(field);

            pointerId = -1;

            if (inkManager.AnySelected())
            {
                inkManager.DeleteSelected();

                // We don't know what has been erased so we clear the render
                // and add back all the ink saved in the ink manager
                renderer.Clear();
                renderer.AddInk(inkManager.GetStrokes());

                // There is no selection - disable movement
                AnchorSelection();
            }
            else
            {
                inkManager.DeleteAll();
                renderer.Clear();
            }
        }

        private void AppBarButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mvm.Preview = "";

            pointerId = -1;

            if (inkManager.AnySelected())
            {
                inkManager.DeleteSelected();

                // We don't know what has been erased so we clear the render
                // and add back all the ink saved in the ink manager
                renderer.Clear();
                renderer.AddInk(inkManager.GetStrokes());

                // There is no selection - disable movement
                AnchorSelection();
            }
            else
            {
                inkManager.DeleteAll();
                renderer.Clear();
            }
        }

        
    }
}
