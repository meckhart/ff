﻿﻿using Bing.Maps;
using Einsatz_Karte.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml;

namespace Einsatz_Karte.Model
{
    public enum RaisePropertyChangedTyp { All, Polylines, Polygons, Circles}
    public enum CheckStopPaintingTyp {Polylines, Polygons, Circles}
    public enum GPSStateTyp { Off, Ready, On}
    public class MapContainer : INotifyPropertyChanged
    {
        #region Properties

        private Settlement _TappedSettlement;
        private Hydrant _TappedHydrant;

        private bool _BTIsArea;
        private bool _BTIsHydrants;
        private bool _BTIsSettlements;
        private bool _BTIsPolygonPainting;
        private bool _BTIsPolylienPainting;
        private bool _BTIsCirclePainting;
        private bool _BTIsGPSOn;
        private DispatcherTimer _GPSTimer;
        private GPSStateTyp _GPSState;

        private ObservableCollection<CustomMapCircle> _Circles;
        private ObservableCollection<CustomMapPolyline> _Polylines;
        private ObservableCollection<CustomMapPolygon> _Polygons;
        private ObservableCollection<Hydrant> _Hydrants;
        private ObservableCollection<Settlement> _Settlements;
        private ObservableCollection<Location> _RouteLocations;
        private ObservableCollection<GPSTarget> _GPSTargets;

        public ObservableCollection<CustomMapCircle> Circles
        {
            get { return _Circles; }
            set { _Circles = value; OnChanged(); }
        }
        public ObservableCollection<PinInfo> CircleInfos
        {
            get
            {
                ObservableCollection<PinInfo> infos = new ObservableCollection<PinInfo>();

                foreach (CustomMapCircle mp in Circles)
                {
                    if (mp.Loc2!=null)
                    {
                        PinInfo info = new PinInfo();
                        info.Loc = mp.Loc1.Locs;
                        MapPolyline line = new MapPolyline();
                        line.Locations.Add(mp.Loc1.Locs);
                        line.Locations.Add(mp.Loc2.Locs);
                        double distance = CalculateDistance(line) * 1000 ;
                        double area = distance * distance * Math.PI;
                        if (area < 10000)
                        {
                            info.Unit = Math.Round(area, 0) + " m²";
                        }
                        else if (area < 1000000)
                        {
                            info.Unit = Math.Round(area / 10000, 2) + " ha";
                        }
                        else
                        {
                            info.Unit = Math.Round(area / 1000000, 2) + " km²";
                        }
                        infos.Add(info);
                    }
                }
                
                return infos;
            }
        }
        public ObservableCollection<PinInfo> CircleInfoRadius
        {
            get
            {
                ObservableCollection<PinInfo> infos = new ObservableCollection<PinInfo>();

                foreach (CustomMapCircle mp in Circles)
                {
                    if (mp.Loc2!=null)
                    {
                        PinInfo info = new PinInfo();
                        info.Loc = mp.Loc2.Locs;
                        MapPolyline line=new MapPolyline();
                        line.Locations.Add(mp.Loc1.Locs);
                        line.Locations.Add(mp.Loc2.Locs);
                        double distance = CalculateDistance(line);
                        if (distance < 1)
                        {
                            info.Unit = Math.Round(distance * 1000, 0) + " m";
                        }
                        else if (distance < 10)
                        {
                            info.Unit = Math.Round(distance, 2) + " km";
                        }
                        else
                        {
                            info.Unit = Math.Round(distance, 1) + " km";
                        }
                        infos.Add(info);
                    }
                }
                
                return infos;
            }
        }
        public ObservableCollection<CustomMapPolyline> Polylines
        {
            get
            {
                return _Polylines;
            }
            set { _Polylines = value; OnChanged(); OnChanged("LinePushPins"); OnChanged("LineInfos"); }
        }
        public ObservableCollection<IDLocation> LinePushPins
        {
            get
            {
                int count1 = 0;
                int count2 = 0;
                ObservableCollection<IDLocation> pins = new ObservableCollection<IDLocation>();
                foreach (CustomMapPolyline mp in Polylines)
                {
                    foreach (Location l in mp.PolyLine.Locations)
                    {
                        pins.Add(new IDLocation("L:" + count1 + ":" + count2, l));
                        count2++;
                    }
                    count2 = 0;
                    count1++;
                }
                return pins;
            }
        }
        public ObservableCollection<PinInfo> LineInfos
        {
            get
            {
                ObservableCollection<PinInfo> infos = new ObservableCollection<PinInfo>();

                foreach (CustomMapPolyline mp in Polylines)
                {
                    if (mp.PolyLine.Locations.Count > 1)
                    {
                        PinInfo info = new PinInfo();
                        info.Loc = mp.PolyLine.Locations[0];
                        double distance = CalculateDistance(mp.PolyLine);
                        if (distance < 1)
                        {
                            info.Unit = Math.Round(distance * 1000, 0) + " m";
                        }
                        else if (distance < 10)
                        {
                            info.Unit = Math.Round(distance, 2) + " km";
                        }
                        else
                        {
                            info.Unit = Math.Round(distance, 1) + " km";
                        }
                        infos.Add(info);
                    }
                }
                
                return infos;
            }
        }
        public ObservableCollection<CustomMapPolygon> Polygons
        {
            get
            {
                return _Polygons;
            }
            set { _Polygons = value; }
        }
        public ObservableCollection<IDLocation> AreaPushPins
        {
            get
            {
                int count1 = 0;
                int count2 = 0;
                ObservableCollection<IDLocation> pins = new ObservableCollection<IDLocation>();
                foreach (CustomMapPolygon mp in Polygons)
                {
                    foreach (Location l in mp.PolyLine.Locations)
                    {
                        pins.Add(new IDLocation("A:" + count1 + ":" + count2, l));
                        count2++;
                    }
                    count2 = 0;
                    count1++;
                }
                return pins;
            }
        }
        public ObservableCollection<PinInfo> AreaInfos
        {
            get
            {
                ObservableCollection<PinInfo> infos = new ObservableCollection<PinInfo>();
                foreach (CustomMapPolygon mp in Polygons)
                {
                    if (mp.PolyLine.Locations.Count > 2)
                    {
                        PinInfo info = new PinInfo();
                        //info.Loc = Compute2DPolygonCentroid(mp);
                        info.Loc = mp.PolyLine.Locations[0];
                        double area = CalculateArea(mp.PolyLine);
                        if (area < 10000)
                        {
                            info.Unit = Math.Round(area, 0) + " m²";
                        }
                        else if (area < 1000000)
                        {
                            info.Unit = Math.Round(area / 10000, 2) + " ha";
                        }
                        else
                        {
                            info.Unit = Math.Round(area / 1000000, 2) + " km²";
                        }
                        infos.Add(info);
                    }
                }
                
                return infos;
            }
        }    
        public ObservableCollection<Hydrant> Hydrants
        {
            get
            {
                return _Hydrants;
            }
            set { _Hydrants = value; }
        }   
        public ObservableCollection<Settlement> Settlements
        {
            get
            {
                return _Settlements;
            }
            set { _Settlements = value; }
        } 
        public Settlement TappedSettlement
        {
            get { return _TappedSettlement; }
            set { _TappedSettlement = value; OnChanged(); }
        }   
        public Hydrant TappedHydrant
        {
            get { return _TappedHydrant; }
            set { _TappedHydrant = value; OnChanged(); }
        }
        public ObservableCollection<Location> RouteLocations
        {
            get { return _RouteLocations; }
            set { _RouteLocations = value; }
        }
        public ObservableCollection<GPSTarget> GPSTargets
        {
            get { return _GPSTargets; }
            set { _GPSTargets = value; }
        }
        public GPSStateTyp GPSState
        {
            get { return _GPSState; }
            set { _GPSState = value; OnChanged("GPSStateColor"); }
        }
        public Windows.UI.Xaml.Media.SolidColorBrush GPSStateColor
        {
            get
            {
                if(GPSState==GPSStateTyp.On)
                {
                    return new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Green);
                }
                else if (GPSState == GPSStateTyp.Ready)
                {
                    return new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Yellow);
                }
                else
                {
                    return new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Red);
                }
            }
        }
        

        public bool BTIsHydrants
        {
            get { return _BTIsHydrants; }
            set { _BTIsHydrants = value; OnChanged("Hydrants"); OnChanged(); OnChanged("BTIsNotHydrants"); }
        }
        public bool BTIsNotHydrants
        {
            get { return !BTIsHydrants; }
        }
        public bool BTIsSettlements
        {
            get { return _BTIsSettlements; }
            set { _BTIsSettlements = value; OnChanged(); OnChanged("BTIsNotSettlements"); OnChanged("Settlements"); }
        }
        public bool BTIsNotSettlements
        {
            get { return !BTIsSettlements; }
        }
        public bool BTIsArea
        {
            get { return _BTIsArea; }
            set { _BTIsArea = value; OnChanged(); OnChanged("BTIsNotArea"); if (value == false) { BTIsPolygonPainting = false; BTIsPolylinePainting = false; } }
        }
        public bool BTIsNotArea
        {
            get { return !BTIsArea; }
        }
        public bool BTIsPolygonPainting
        {
            get { return _BTIsPolygonPainting; }
            set { _BTIsPolygonPainting = value; OnChanged(); OnChanged("BTIsNotPolygonPainting"); }
        }
        public bool BTIsNotPolygonPainting
        {
            get { return !BTIsPolygonPainting; }
        }
        public bool BTIsPolylinePainting
        {
            get { return _BTIsPolylienPainting; }
            set { _BTIsPolylienPainting = value; OnChanged(); OnChanged("BTIsNotPolylinePainting"); }
        }
        public bool BTIsNotPolylinePainting
        {
            get { return !BTIsPolylinePainting; }
        }
        public bool BTIsCirclePainting
        {
            get { return _BTIsCirclePainting; }
            set { _BTIsCirclePainting = value; OnChanged(); OnChanged("BTIsNotCirclePainting"); }
        }
        public bool BTIsNotCirclePainting
        {
            get { return !BTIsCirclePainting; }
        }
        public bool BTIsGPSOn
        {
            get { return _BTIsGPSOn; }
            set { _BTIsGPSOn = value; OnChanged(); OnChanged("BTIsNotGPSOn"); OnChanged("GPSTargets"); SetGPS(); }
        }
        public bool BTIsNotGPSOn
        {
            get { return !BTIsGPSOn; }
        }
        
        public MapContainer()
        {
            Circles = new ObservableCollection<CustomMapCircle>();
            Polylines = new ObservableCollection<CustomMapPolyline>();
            Polygons = new ObservableCollection<CustomMapPolygon>();
            Hydrants = new ObservableCollection<Hydrant>();
            Settlements = new ObservableCollection<Settlement>();
            RouteLocations = new ObservableCollection<Location>();
            GPSTargets = new ObservableCollection<GPSTarget>();
            GPSState = GPSStateTyp.Off;

            BTIsHydrants = true;
            BTIsSettlements = true;
            BTIsArea = true;
            BTIsPolygonPainting = false;
            BTIsPolylinePainting = false;
            BTIsCirclePainting = false;
            BTIsGPSOn = false;

            _GPSTimer = new DispatcherTimer();
            _GPSTimer.Tick += OnUpdateGPS;
            _GPSTimer.Interval = new TimeSpan(0, 0, 0);
            App.Current.Suspending+=OnAppClosed;
        }

        


        #endregion Properties

        #region Getters
        public IEnumerable<MapPolyline> GetAllPolylines()
        {
            if (BTIsNotArea)
                return new List<MapPolyline>();
            List<MapPolyline> mps = new List<MapPolyline>();
            foreach(CustomMapPolyline mp in Polylines)
            {
                mps.Add(mp.PolyLine);
            }
            return mps;
        }

        public IEnumerable<MapPolygon> GetAllPolygons()
        {
            List<MapPolygon> mps = new List<MapPolygon>();
            if (BTIsNotArea)
                return new List<MapPolygon>();
            foreach (CustomMapPolygon mp in Polygons)
            {
                mps.Add(mp.PolyLine);
            }
            return mps;
        }

        public IEnumerable<CustomMapCircle> GetAllCircles()
        {
            if (BTIsNotArea)
                return new List<CustomMapCircle>();
            return Circles;
        }
        #endregion Getters

        #region Calculation
        private double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }
        public IEnumerable<Location> CalcCircle(CustomMapCircle circle)
        {
            int nrOfPoints = 360;
            Location center = circle.Loc1.Locs;
            double radius = DistanceTo(circle.Loc1.Locs.Latitude, circle.Loc1.Locs.Longitude, circle.Loc2.Locs.Latitude, circle.Loc2.Locs.Longitude);
            radius = radius * 1000;
            var angle = 360.0 / nrOfPoints;
            var locations = new List<Location>();
            for (var i = 0; i <= nrOfPoints; i++)
            {
                locations.Add(GetAtDistanceBearing(center, radius, angle * i));
            }
            return locations;
        }
        private Location GetAtDistanceBearing(Location point, double distance, double bearing)
        {
            const double degreesToRadian = Math.PI / 180.0;
            const double radianToDegrees = 180.0 / Math.PI;
            const double earthRadius = 6378137.0;

            var latA = point.Latitude * degreesToRadian;
            var lonA = point.Longitude * degreesToRadian;
            var angularDistance = distance / earthRadius;
            var trueCourse = bearing * degreesToRadian;

            var lat = Math.Asin(
                Math.Sin(latA) * Math.Cos(angularDistance) +
                Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

            var dlon = Math.Atan2(
                Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA),
                Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));

            var lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

            var result = new Location(lat * radianToDegrees, lon * radianToDegrees);

            return result;
        }

        private double CalculateArea(MapPolygon mp)
        {
            List<Double> lats = new List<double>();
            List<Double> lons = new List<double>();
            foreach (Location l in mp.Locations)
            {
                lats.Add(l.Latitude);
                lons.Add(l.Longitude);
            }

            double sum = 0;
            double prevcolat = 0;
            double prevaz = 0;
            double colat0 = 0;
            double az0 = 0;
            for (int i = 0; i < lats.Count; i++)
            {
                double colat = 2 * Math.Atan2(Math.Sqrt(Math.Pow(Math.Sin(lats[i] * Math.PI / 180 / 2), 2) + Math.Cos(lats[i] * Math.PI / 180) * Math.Pow(Math.Sin(lons[i] * Math.PI / 180 / 2), 2)), Math.Sqrt(1 - Math.Pow(Math.Sin(lats[i] * Math.PI / 180 / 2), 2) - Math.Cos(lats[i] * Math.PI / 180) * Math.Pow(Math.Sin(lons[i] * Math.PI / 180 / 2), 2)));
                double az = 0;
                if (lats[i] >= 90)
                {
                    az = 0;
                }
                else if (lats[i] <= -90)
                {
                    az = Math.PI;
                }
                else
                {
                    az = Math.Atan2(Math.Cos(lats[i] * Math.PI / 180) * Math.Sin(lons[i] * Math.PI / 180), Math.Sin(lats[i] * Math.PI / 180)) % (2 * Math.PI);
                }
                if (i == 0)
                {
                    colat0 = colat;
                    az0 = az;
                }
                if (i > 0 && i < lats.Count)
                {
                    sum = sum + (1 - Math.Cos(prevcolat + (colat - prevcolat) / 2)) * Math.PI * ((Math.Abs(az - prevaz) / Math.PI) - 2 * Math.Ceiling(((Math.Abs(az - prevaz) / Math.PI) - 1) / 2)) * Math.Sign(az - prevaz);
                }
                prevcolat = colat;
                prevaz = az;
            }
            sum = sum + (1 - Math.Cos(prevcolat + (colat0 - prevcolat) / 2)) * (az0 - prevaz);
            return 5.10072E14 * Math.Min(Math.Abs(sum) / 4 / Math.PI, 1 - Math.Abs(sum) / 4 / Math.PI);
        }

        public double CalculateDistance(MapPolyline mp)
        {
            double distance = 0;

            for (int i = 0; i < mp.Locations.Count - 1; i++)
            {
                distance += DistanceTo(mp.Locations[i].Latitude, mp.Locations[i].Longitude, mp.Locations[i + 1].Latitude, mp.Locations[i + 1].Longitude);
            }
            return distance;
        }

        #endregion Calcuation

        #region INotifyPropertyChanged
        public void RaisePropertyChanged(RaisePropertyChangedTyp typ)
        {
            if (typ == RaisePropertyChangedTyp.Polylines)
            {
                OnChanged("LinePushPins"); 
                OnChanged("LineInfos");
            }
            else if(typ == RaisePropertyChangedTyp.Polygons)
            {
                OnChanged("AreaPushPins");
                OnChanged("AreaInfos");
            }
            else if (typ == RaisePropertyChangedTyp.Circles)
            {
                OnChanged("CircleInfos");
                OnChanged("CircleInfoRadius");
            }
            else if (typ == RaisePropertyChangedTyp.All)
            {
                OnChanged("LinePushPins");
                OnChanged("LineInfos");
                OnChanged("AreaPushPins");
                OnChanged("AreaInfos");
                OnChanged("Circles");
                OnChanged("CircleInfos");
                OnChanged("CircleInfoRadius");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }

        #endregion INotifyPropertyChanged

        #region Helpers

        public void CheckStopPainting(CheckStopPaintingTyp typ)
        {
            if(typ==CheckStopPaintingTyp.Circles)
            {
                if(Circles.Last().Loc2==null)
                {
                    Circles.Remove(Circles.Last());
                    RaisePropertyChanged(RaisePropertyChangedTyp.Circles);
                }
            }
            else if (typ == CheckStopPaintingTyp.Polygons)
            {
                if (Polygons.Last().PolyLine.Locations.Count<3)
                {
                    Polygons.Remove(Polygons.Last());
                    RaisePropertyChanged(RaisePropertyChangedTyp.Polygons);
                }
            }
            else if (typ == CheckStopPaintingTyp.Polylines)
            {
                if (Polylines.Last().PolyLine.Locations.Count<2 == null)
                {
                    Polylines.Remove(Polylines.Last());
                    RaisePropertyChanged(RaisePropertyChangedTyp.Polylines);
                }
            }
        }

        #endregion Helpers

        #region GPS
        private void SetGPS()
        {
            if (_GPSTimer != null)
            {
                if (BTIsGPSOn)
                {
                    GPSState = GPSStateTyp.Ready;
                    _GPSTimer.Interval = new TimeSpan(0, 0, 0);
                    _GPSTimer.Start();
                }
                else
                {
                    _GPSTimer.Stop();
                    GPSState = GPSStateTyp.Off;
                    GPSTargets = new ObservableCollection<GPSTarget>();
                }
            }
        }
        private async void OnUpdateGPS(object sender, object e)
        {
            try
            {
                ((DispatcherTimer)sender).Interval = new TimeSpan(0, 0, 5);
                //Set RLF
                Geolocator _GeoLocator = new Geolocator();
                Geoposition _GeoPosition = await _GeoLocator.GetGeopositionAsync();
                if (GPSTargets.Count == 0)
                {
                    GPSTargets.Add(new GPSTarget("RLF"));
                }
                GPSTargets.First(p => p.Title == "RLF").Latitude = _GeoPosition.Coordinate.Point.Position.Latitude;
                GPSTargets.First(p => p.Title == "RLF").Longitude = _GeoPosition.Coordinate.Point.Position.Longitude;
                GPSState = GPSStateTyp.On;
            }
            catch
            {
                BTIsGPSOn = false;
                GPSState = GPSStateTyp.Off;
            }

        }
        private void OnAppClosed(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            _GPSTimer.Stop();
        }

        #endregion GPS
    }
}
