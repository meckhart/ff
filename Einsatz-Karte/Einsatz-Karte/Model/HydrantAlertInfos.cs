﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class HydrantAlertInfos : INotifyPropertyChanged
    {
        private double _AirDistanceToAlert;
        private double _WayDistanceToAlert;

        public double AirDistanceToAlert
        {
            get { return _AirDistanceToAlert; }
            set { _AirDistanceToAlert = value; OnChanged("AirDistanceToAlert_String"); }
        }

        public double WayDistanceToAlert
        {
            get { return _WayDistanceToAlert; }
            set { _WayDistanceToAlert = value; OnChanged("WayDistanceToAlert_String"); }
        }


        public string AirDistanceToAlert_String
        {
            get
            {
                if (AirDistanceToAlert == 0)
                {
                    return "???";
                }
                else
                {
                    if (AirDistanceToAlert < 1)
                    {
                        return Math.Round(AirDistanceToAlert * 1000, 0) + " m";
                    }
                    else if (AirDistanceToAlert < 10)
                    {
                        return Math.Round(AirDistanceToAlert, 2) + " km";
                    }
                    else
                    {
                        return Math.Round(AirDistanceToAlert, 1) + " km";
                    }
                }
            }
        }
        public string WayDistanceToAlert_String
        {
            get
            {
                if (WayDistanceToAlert == 0)
                {
                    return "???";
                }
                else
                {
                    if (WayDistanceToAlert < 1)
                    {
                        return Math.Round(WayDistanceToAlert * 1000, 0) + " m";
                    }
                    else if (AirDistanceToAlert < 10)
                    {
                        return Math.Round(WayDistanceToAlert, 2) + " km";
                    }
                    else
                    {
                        return Math.Round(WayDistanceToAlert, 1) + " km";
                    }
                }
            }
        }

        public HydrantAlertInfos()
        {
            AirDistanceToAlert = 0;
            WayDistanceToAlert = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
