﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class CustomMapCircle : INotifyPropertyChanged
    {
        public CustomMapCircle()
        {

        }

        private IDLocation _Loc1;

        public IDLocation Loc1
        {
            get { return _Loc1; }
            set { _Loc1 = value; OnChanged(); }
        }

        private IDLocation _Loc2;

        public IDLocation Loc2
        {
            get { return _Loc2; }
            set { _Loc2 = value; OnChanged(); }
        }

        private MapPolygon _Circle;

        public MapPolygon Circle
        {
            get { return _Circle; }
            set { _Circle = value; OnChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
        
    }
}
