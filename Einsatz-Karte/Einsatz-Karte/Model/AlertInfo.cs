﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public enum AlertStatus {NoAlert, Alert, OldAlert}
    public class AlertInfo : INotifyPropertyChanged
    {
        private string _Place1;
        private string _Place2;
        private string _OriginalAddress;
        private AlertStatus _Status;
        private DateTime _AlertDateTime;
        private Bing.Maps.Location _AlertLocation;

        public string Place1
        {
            get { return _Place1; }
            set { _Place1 = value; }
        }
        public string Place2
        {
            get { return _Place2; }
            set { _Place2 = value; }
        }
        public string OriginalAddress
        {
            get { return _OriginalAddress; }
            set { _OriginalAddress = value; }
        }
        public AlertStatus Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }
        public DateTime AlertDateTime
        {
            get { return _AlertDateTime; }
            set { _AlertDateTime = value; }
        }
        public Bing.Maps.Location AlertLocation
        {
            get { return _AlertLocation; }
            set { _AlertLocation = value; }
        }
        
        
        public AlertInfo()
        {
            Status = AlertStatus.NoAlert;
        }
        public AlertInfo(string place1, string place2)
        {
            Place1 = place1;
            Place2 = place2;
            OriginalAddress = "";
            Status = AlertStatus.Alert;
        }
        public AlertInfo(string place1, string place2, string originalAddress, DateTime alertDateTime)
        {
            Place1 = place1;
            Place2 = place2;
            OriginalAddress = originalAddress;
            Status = AlertStatus.Alert;
            AlertDateTime = alertDateTime;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
