﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class Settlement : INotifyPropertyChanged
    {
        private string _GUID;

        [DataMember]
        public string GUID
        {
            get { return _GUID; }
            set { _GUID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; OnChanged(); }
        }
        
        private string _Address;

        [DataMember]
        public string Address
        {
            get { return _Address; }
            set { _Address = value; OnChanged(); }
        }

        private double _Latitude;

        [DataMember]
        public double Latitude
        {
            get { return _Latitude; }
            set { _Latitude = value; OnChanged(); }
        }

        private double _Longitude;

        [DataMember]
        public double Longitude
        {
            get { return _Longitude; }
            set { _Longitude = value; OnChanged(); }
        }

        private string _ImageURL;

        [DataMember]
        public string ImageUrl
        {
            get { return _ImageURL; }
            set { _ImageURL = value; OnChanged(); }
        }

        private ObservableCollection<SettlementInfo> _Info;

        public ObservableCollection<SettlementInfo> Info
        {
            get { return _Info; }
            set { _Info = value; OnChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }

    public class SettlementInfo : INotifyPropertyChanged
    {
        private string _Title;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; OnChanged(); }
        }

        private string _Value;

        public string Value
        {
            get { return _Value; }
            set { _Value = value; OnChanged(); }
        }

        public override string ToString()
        {
            return Title;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
