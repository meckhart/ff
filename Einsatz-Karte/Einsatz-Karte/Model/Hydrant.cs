﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    [DataContract]
    public class Hydrant :INotifyPropertyChanged
    {
        private string _GUID;

        [DataMember]
        public string GUID
        {
            get { return _GUID; }
            set { _GUID = value; }
        }
        

        private string _Description;

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; OnChanged(); }
        }

        private string _Address;

        [DataMember]
        public string Address
        {
            get { return _Address; }
            set { _Address = value; OnChanged(); }
        }

        private double _Latitude;

        [DataMember]
        public double Latitude
        {
            get { return _Latitude; }
            set { _Latitude = value; OnChanged(); }
        }

        private double _Longitude;

        [DataMember]
        public double Longitude
        {
            get { return _Longitude; }
            set { _Longitude = value; OnChanged(); }
        }

        private string _ImageURL;

        [DataMember]
        public string ImageUrl
        {
            get { return _ImageURL; }
            set { _ImageURL = value; OnChanged(); }
        }

        private int _C;
        [DataMember]
        public int C
        {
            get { return _C; }
            set { _C = value; OnChanged(); }
        }

        private int _B;

        [DataMember]
        public int B
        {
            get { return _B; }
            set { _B = value; OnChanged(); }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
