﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class Town
    {
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _Type;

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private int _PostalCode;

        public int PostalCode
        {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }

        private List<Way> _Ways;

        public List<Way> Ways
        {
            get { return _Ways; }
            set { _Ways = value; }
        }
        
        
    }
}
