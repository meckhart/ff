﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class ManuelRoute : INotifyPropertyChanged
    {
        private string _Location1;
        private string _Location2;
        private double _TravelDuration;
        private double _TravelDistance;

        public string Location1
        {
            get { return _Location1; }
            set { _Location1 = value; OnChanged(); }
        }
        public string Location2
        {
            get { return _Location2; }
            set { _Location2 = value; OnChanged(); OnChanged("IsRouteReady"); }      
        }
        public bool IsRouteReady
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Location2))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public double TravelDuration
        {
            get { return _TravelDuration; }
            set { _TravelDuration = value; OnChanged("TravelDuration_String"); OnChanged("IsTravelInformation"); }
        }
        public double TravelDistance
        {
            get { return _TravelDistance; }
            set { _TravelDistance = value; OnChanged("TravelDistance_String"); OnChanged("IsTravelInformation"); }
        }
        public string TravelDuration_String
        {
            get { return "Die Zeit für die Strecke beträgt " + GetTimeFormatFromSeconds(_TravelDuration); }
        }
        public string TravelDistance_String
        {
            get { return "Die Entfernung der Strecke beträgt "+Math.Round(_TravelDistance,2) + " km"; }
        }
        public bool IsTravelInformation
        {
            get
            {
                if (_TravelDistance > 0 && _TravelDuration > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        private string GetTimeFormatFromSeconds(double seconds)
        {
            TimeSpan timeSpan = new TimeSpan(0, 0,(int) seconds);
            int h = timeSpan.Hours;
            int m = timeSpan.Minutes;
            int s = timeSpan.Seconds;
            if (h > 0 && m > 0 && s > 0)
            {
                return string.Format("{0} Stunden, {1} Minuten und {2} Sekunden", h, m, s);
            }
            else if (m > 0 && s > 0)
            {
                return string.Format("{0} Minuten und {1} Sekunden", m, s);
            }
            else if (h > 0 && s > 0)
            {
                return string.Format("{0} Stunden und {1} Sekunden", h, s);
            }
            else if (h > 0 && m > 0)
            {
                return string.Format("{0} Stunden und {1} Minuten", m, s);
            }
            else if (h > 0)
            {
                return string.Format("{0} Stunden", h);
            }
            else if (m > 0)
            {
                return string.Format("{0} Minuten", m);
            }
            else if (s > 0)
            {
                return string.Format("{0} Sekunden", s);
            }
            return "";
            
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
