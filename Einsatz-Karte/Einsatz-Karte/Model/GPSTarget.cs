﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class GPSTarget : INotifyPropertyChanged
    {
        private double _Latitude;
        private double _Longitude;
        private string _Title;

        public double Latitude
        {
            get { return _Latitude; }
            set { _Latitude = value; OnChanged(); }
        }
        public double Longitude
        {
            get { return _Longitude; }
            set { _Longitude = value; OnChanged(); }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; OnChanged(); }
        }

        public GPSTarget(string title)
        {
            Title = title;
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
