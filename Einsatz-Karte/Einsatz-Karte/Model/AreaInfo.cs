﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class PinInfo
    {
        private string _Unit;

        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        private Location _Loc;

        public Location Loc
        {
            get { return _Loc; }
            set { _Loc = value; }
        }
        
        
    }
}
