﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Einsatz_Karte.Model
{
    public class ActionCommand : ICommand
    {
        private readonly Action<object> _executeHandler;
        private readonly Func<object, bool> _canexecuteHandler;

        public ActionCommand(Action<object> execute, Func<object, bool> canexecute)
        {
            if (execute == null)
                throw new ArgumentNullException("Execute cannot be null");
            _executeHandler = execute;
            _canexecuteHandler = canexecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canexecuteHandler == null)
            {
                return true;
            }
            return _canexecuteHandler(parameter);
        }

        public void Execute(object parameter)
        {
            _executeHandler(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
