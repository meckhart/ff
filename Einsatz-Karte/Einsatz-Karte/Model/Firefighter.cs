﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class Firefighter : INotifyPropertyChanged
    {
        private String _Firstname;

        [DataMember]
        public String Firstname
        {
            get { return _Firstname; }
            set { _Firstname = value; }
        }

        private String  _Lastname;

        [DataMember]
        public String  Lastname
        {
            get { return _Lastname; }
            set { _Lastname = value; }
        }

        private String _Rank;

        [DataMember]
        public String Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }

        private String _ImageUrl;

        [DataMember]
        public String ImageUrl
        {
            get { return _ImageUrl; }
            set { _ImageUrl = value; }
        }

        private bool _IsTapped;

        public bool IsTapped
        {
            get { return _IsTapped; }
            set { _IsTapped = value; OnChanged(); }
        }
        
        public void ChangeTapped()
        {
            if (IsTapped)
                IsTapped = false;
            else
                IsTapped = true;
        }

        private String _VehiclesAllowed;

        [DataMember]
        public String VehiclesAllowed
        {
            get { return _VehiclesAllowed; }
            set { _VehiclesAllowed = value; }
        }

        public String DisplayName
        {
            get
            {
                return Lastname + " " + Firstname;
            }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }

        
    }
}
