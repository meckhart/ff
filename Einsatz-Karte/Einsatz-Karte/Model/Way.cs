﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class Way
    {
        private double _ID;

        public double ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private List<double> _RefValues;

        public List<double> RefValues
        {
            get { return _RefValues; }
            set { _RefValues = value; }
        }

        public Way(double id, List<double> refValues)
        {
            ID = id;
            RefValues = refValues;
        }
        
    }
}
