﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace Einsatz_Karte.Model
{
    public class PLine : INotifyPropertyChanged
    {
        private LocationCollection _Locations;

        public LocationCollection Locations
        {
            get { return _Locations; }
            set { _Locations = value; }
        }

        private Color _LineColor;

        public Color LineColor
        {
            get { return _LineColor; }
            set { _LineColor = value; OnChanged(); }
        }
        

        public PLine()
        {
            Locations = new LocationCollection();
        }

        public void SetLocation(IEnumerable<Location> locs)
        {
            foreach (Location loc in locs)
            {
                Locations.Add(loc);
            }
            OnChanged("Locations");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
        
    }
}
