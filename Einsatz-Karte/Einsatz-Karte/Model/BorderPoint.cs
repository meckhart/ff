﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class BorderPoint
    {
        private double _ID;

        public double ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private double _Lat;

        public double Lat
        {
            get { return _Lat; }
            set { _Lat = value; }
        }

        private double _Lon;

        public double Lon
        {
            get { return _Lon; }
            set { _Lon = value; }
        }
        
    }
}
