﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model.BingMaps
{
    public class RoutePropertiesResponse
    {
        public List<Location> RouteLocations { get; set; }
        public double TravelDuration { get; set; }
        public double TravelDistance { get; set; }

        public RoutePropertiesResponse()
        {
            RouteLocations = new List<Location>();
        }
    }
}
