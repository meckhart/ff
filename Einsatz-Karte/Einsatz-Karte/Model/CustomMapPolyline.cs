﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class CustomMapPolyline : INotifyPropertyChanged
    {
        private MapPolyline _PolyLine;

        public MapPolyline PolyLine
        {
            get { return _PolyLine; }
            set { _PolyLine = value; OnChanged(); }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
