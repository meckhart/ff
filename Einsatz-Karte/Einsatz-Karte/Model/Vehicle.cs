﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    [DataContract]
    public class Vehicle : INotifyPropertyChanged
    {
        private string _Name;

        [DataMember]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _ImageUrl;

        [DataMember]
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set { _ImageUrl = value; }
        }

        private bool _IsTapped;

        public bool IsTapped
        {
            get { return _IsTapped; }
            set { _IsTapped = value; OnChanged(); if (value) { SelectDriver(); } else { Driver = null; } }
        }

        public void ChangeTapped()
        {
            if (IsTapped)
                IsTapped = false;
            else
                IsTapped = true;
        }

        private int _KM;

        public int KM
        {
            get { return _KM; }
            set { if (value > -1) { _KM = value; } else { _KM = 0; } OnChanged(); }
        }

        private Firefighter _Driver;

        public Firefighter Driver
        {
            get { return _Driver; }
            set { _Driver = value; OnChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
        
        private async void SelectDriver()
        {
            Einsatz_Karte.CustomControls.DriverPicker cp = new Einsatz_Karte.CustomControls.DriverPicker();
            Firefighter ff = await cp.ShowAsync(this);
            if (ff != null)
            {
                Driver = await cp.ShowAsync(this);
            }
            else
            {
                IsTapped = false;
            }

        }
        
    }
}
