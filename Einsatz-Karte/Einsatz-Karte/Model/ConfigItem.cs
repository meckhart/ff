﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    [DataContract]
    public class ConfigItem : INotifyPropertyChanged
    {
        private string _ConfigName;

        [DataMember]
        public string ConfigName
        {
            get { return _ConfigName; }
            set { _ConfigName = value; OnChanged(); }
        }

        private string _Value;

        [DataMember]
        public string Value
        {
            get { return _Value; }
            set { _Value = value; OnChanged(); }
        }

        public ConfigItem() { }

        public ConfigItem(string configName, string value)
        {
            ConfigName = configName;
            Value = value;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
