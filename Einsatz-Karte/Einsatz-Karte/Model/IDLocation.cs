﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class IDLocation : INotifyPropertyChanged 
    {
        public IDLocation(string id, Location locs)
        {
            ID=id;
            Locs = locs;
        }

        private Location _Locs;

        public Location Locs
        {
            get { return _Locs; }
            set { _Locs = value; OnChanged(); }
        }

        private String  _ID;

        public String ID
        {
            get { return _ID; }
            set { _ID = value; OnChanged(); }
        }   

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
    }
}
