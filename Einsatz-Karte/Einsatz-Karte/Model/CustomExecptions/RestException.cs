﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model.CustomExecptions
{
    public enum RestExceptionType { Common, NoKeyInConfig, NoConnection, NoLocationFound, NoRouteFound, OverQqueryLimit }
    public class RestException : Exception
    {
        public RestExceptionType RestExceptionType { get; set; }

        public RestException() : base()
        {
            RestExceptionType = RestExceptionType.Common;
        }

        public RestException(RestExceptionType restExceptionType, string message)
            : base(message)
        {
            RestExceptionType = restExceptionType;
        }

        public RestException(RestExceptionType restExceptionType, string message, Exception innerException)
            : base(message, innerException)
        {
            RestExceptionType = restExceptionType;
        }
    }
}
