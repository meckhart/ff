﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class Report : INotifyPropertyChanged
    {
        public Report()
        {
            Date = DateTime.Now;
            AlertTime = DateTime.Now;
            EndTime = DateTime.Now;
        }

        private DateTime _Date;

        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; OnChanged(); }
        }

        private DateTime _AlertTime;

        public DateTime AlertTime
        {
            get { return _AlertTime; }
            set { _AlertTime = value; OnChanged(); }
        }

        private DateTime _EndTime;

        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; OnChanged(); }
        }

        private bool _IsFireAlert;

        public bool IsFireAlert
        {
            get { return _IsFireAlert; }
            set { _IsFireAlert = value; OnChanged(); }
        }

        private bool _IsTechniqueAlert;

        public bool IsTechniqueAlert
        {
            get { return _IsTechniqueAlert; }
            set { _IsTechniqueAlert = value; OnChanged(); }
        }

        private bool _IsFalseAlert;

        public bool IsFalseAlert
        {
            get { return _IsFalseAlert; }
            set { _IsFalseAlert = value; OnChanged(); }
        }

        private bool _IsOtherAlert;

        public bool IsOtherAlert
        {
            get { return _IsOtherAlert; }
            set { _IsOtherAlert = value; OnChanged(); }
        }

        private string _OtherAlert;

        public string OtherAlert
        {
            get { return _OtherAlert; }
            set { _OtherAlert = value; OnChanged(); }
        }

        private string _UseLocation;

        public string UseLocation
        {
            get { return _UseLocation; }
            set { _UseLocation = value; OnChanged(); }
        }

        private string _UseStreet;

        public string UseStreet
        {
            get { return _UseStreet; }
            set { _UseStreet = value; OnChanged(); }
        }

        private string _MissionControlBrigade;

        public string MissionControlBrigade
        {
            get { return _MissionControlBrigade; }
            set { _MissionControlBrigade = value; OnChanged(); }
        }

        private string _Owner;

        public string Owner
        {
            get { return _Owner; }
            set { _Owner = value; OnChanged(); }
        }

        private string _OwnerLocation;

        public string OwnerLocation
        {
            get { return _OwnerLocation; }
            set { _OwnerLocation = value; OnChanged(); }
        }
        

        private int _PLZ;

        public int PLZ
        {
            get { return _PLZ; }
            set { _PLZ = value; OnChanged(); }
        }

        private string _LicensePlate;

        public string LicensePlate
        {
            get { return _LicensePlate; }
            set { _LicensePlate = value; OnChanged(); }
        }

        private string _Vulgo;

        public string Vulgo
        {
            get { return _Vulgo; }
            set { _Vulgo = value; OnChanged(); }
        }

        private string _OwnerStreet;

        public string OwnerStreet
        {
            get { return _OwnerStreet; }
            set { _OwnerStreet = value; OnChanged(); }
        }

        private string _Info;

        public string Info
        {
            get { return _Info; }
            set { _Info = value; OnChanged(); }
        }
        
        







        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            Einsatz_Karte.Service.ReportService.AlertReport = this;
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }
        
    }
}
