﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Einsatz_Karte.Model.APIXML
{
    public class GeocodeResponse
    {
        [XmlElement("status")]
        public String Status { get; set; }

        [XmlElement("result")]
        public Result Result { get; set; }

        [XmlElement("error_message")]
        public string ErrorMessage { get; set; }
    }

    public class Result
    {
        [XmlElement("type")]
        public String Status { get; set; }

        [XmlElement("formatted_address")]
        public String FomrattedAddress { get; set; }

        [XmlElement("address_component")]
        public List<AddressComponent> AddressComponent { get; set; }

        [XmlElement("geometry")]
        public Geometry Geometry { get; set; }
    }

    public class AddressComponent
    {
        [XmlElement("long_name")]
        public String LongName { get; set; }

        [XmlElement("short_name")]
        public String ShortName { get; set; }

        [XmlElement("type")]
        public List<String> Type { get; set; }
    }

    public class Geometry
    {
        [XmlElement("location")]
        public GoogleLocation Location { get; set; }
    }

    public class GoogleLocation
    {

        [XmlElement("lat")]
        public double Lat { get; set; }

        [XmlElement("lng")]
        public double Lng { get; set; }
    }
}
