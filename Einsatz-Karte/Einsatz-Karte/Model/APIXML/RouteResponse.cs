﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Einsatz_Karte.Model.APIXML
{
    [XmlRoot(ElementName = "Response")]
    public class RouteResponse
    {
        [XmlElement("StatusDescription")]
        public string StatusDescription { get; set; }

        [XmlElement("AuthenticationResultCode")]
        public string AuthenticationResultCode { get; set; }

        [XmlElement("ResourceSets")]
        public ResourceSets ResourceSets { get; set; }
    }

    public class ResourceSets
    {
        [XmlElement("ResourceSet")]
        public List<ResourceSet> ResourceSetCollection { get; set; }
    }

    public class ResourceSet
    {
        [XmlElement("EstimatedTotal")]
        public int EstimatedTotal { get; set; }

        [XmlElement("Resources")]
        public List<Resources> Resources { get; set; }
    }

    public class Resources
    {
        [XmlElement("Route")]
        public List<Route> RouteCollection { get; set; }
    }

    public class Route
    {
        [XmlElement("DistanceUnit")]
        public string DistanceUnit { get; set; }

        [XmlElement("DurationUnit")]
        public string DurationUnit { get; set; }

        [XmlElement("TravelDistance")]
        public double TravelDistance { get; set; }

        [XmlElement("TravelDuration")]
        public int TravelDuration { get; set; }

        [XmlElement("RoutePath")]
        public RoutePath RoutePath { get; set; }
    }

    public class RoutePath
    {
        [XmlElement("Line")]
        public List<Line> RoutePathCollection { get; set; }
    }

    public class Line
    {
        [XmlElement("Point")]
        public List<Point> Points {get;set;}
    }

    public class Point
    {
        [XmlElement("Latitude")]
        public double Latitude { get; set; }

        [XmlElement("Longitude")]
        public double Longitude { get; set; }
    }
}
