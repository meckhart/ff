﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Einsatz_Karte.Model
{
    public class ImageLocation : INotifyPropertyChanged 
    {
        public ImageLocation(string path)
        {
            Path = path;
        }

        private double _Lat;

        public double Lat
        {
            get { return _Lat; }
            set { _Lat = value; OnMarshalledChanged(); }
        }
        

        private double _Lon;

        public double Lon
        {
            get { return _Lon; }
            set { _Lon = value; OnMarshalledChanged(); }
        }
        

        private String  _Path;

        public String Path
        {
            get { return _Path; }
            set { _Path = value; OnMarshalledChanged(); }
        } 
  
        public void SetLocation(double lat, double lon)
        {
            Lat = lat;
            Lon = lon;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(s));
        }

        protected void OnMarshalledChanged([CallerMemberName] string s = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                    () =>
                    {
                        handler(this, new PropertyChangedEventArgs(s));
                    });

            }
        }
    }
}
