﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Einsatz_Karte.Model;
using Einsatz_Karte.Service;

namespace Einsatz_Karte.ViewModel
{
    public class SettlementsViewModel : BaseViewModel
    {
        private ObservableCollection<Settlement> _Settlements;

        public ObservableCollection<Settlement> Settlements
        {
            get { return _Settlements; }
            set { _Settlements = value; OnChanged(); }
        }

        public SettlementsViewModel()
        {
            LoadSettlements();
        }

        private async void LoadSettlements()
        {
            Settlements = new ObservableCollection<Settlement>(await FileService.GetSettlementsXML());
        }
        
    }
}
