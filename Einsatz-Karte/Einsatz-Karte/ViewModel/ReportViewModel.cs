﻿using Einsatz_Karte.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Printing;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Printing;


namespace Einsatz_Karte.ViewModel
{
    public class ReportViewModel : BaseViewModel
    {
        //public event Einsatz_Karte.View.SetHTMLOnViewHandler SetHTMLOnView1;
        //public event Einsatz_Karte.View.SetHTMLOnViewHandler SetHTMLOnView2;

        private String _HtmlContent1;

        public String HtmlContent1
        {
            get { return _HtmlContent1; }
            set { _HtmlContent1 = value;  }
        }

        private string _HtmlContent2;

        public string HtmlContent2
        {
            get { return _HtmlContent2; }
            set { _HtmlContent2 = value;}
        }
        
        

        public ReportViewModel()
        {
            //SetData();
        }

        private async void SetData()
        {
            HtmlContent1 = await ReportService.GetHtmlForReportPage1();
            HtmlContent2 = await ReportService.GetHtmlForReportPage2();
        }
    
  
        
    }
}
