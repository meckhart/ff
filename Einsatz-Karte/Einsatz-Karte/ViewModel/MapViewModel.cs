﻿using Bing.Maps;
using Einsatz_Karte.CustomControls;
using Einsatz_Karte.Model;
using Einsatz_Karte.Model.BingMaps;
using Einsatz_Karte.Model.CustomExecptions;
using Einsatz_Karte.Service;
using Einsatz_Karte.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Core;
using Windows.Data.Xml.Dom;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage.Pickers;
using Windows.UI.Core;
using Windows.UI.Popups;

namespace Einsatz_Karte.ViewModel
{
    public class MapViewModel : BaseViewModel
    {
        #region Events
        public event SetRouteHandlerView SetRouteView;
        public event SetRouteHandlerViewLocation SetRouteViewLocation;
        public event RemoveRouteFromMapHandler RemoveRouteFromMap;
        public event SetPolygonsHandler SetPolygons;
        public event SetPolylinesHandler SetPolylines;
        public event SetCircleHandler SetCircles;
        public event EventHandler RefreshMap;
        public event SetPolylinesHandler SetTowns;
        #endregion Events

        #region Commands
        public ICommand NewManuelRoute { get; private set; }
        public ICommand CheckIfAlert { get; private set; }
        public ICommand NewHydrantRoute { get; private set; }
        public ICommand OpenAlarmFax { get; private set; }
        public ICommand QuiteAlert { get; private set; }
        public ICommand ChangeAlertLocation { get; private set; }
        public ICommand SetAreaState { get; set; }
        public ICommand SetStackPanel { get; set; }
        public ICommand AlertActions { get; set; }
        public ICommand HydrantActions { get; set; }
        public ICommand SettlementActions { get; set; }
        public ICommand RouteActions { get; set; }
        public ICommand MapToolsActions { get; set; }
        public ICommand ExternToolsActions { get; set; }
        public ICommand TeamActions { get; set; }
        public ICommand TownsActions { get; set; }
        public ICommand HydrantCalculateAction { get; set; }
        public ICommand ManuelRouteAction { get; set; }

        public ICommand GPSAction { get; set; }
        #endregion Commands


        #region Properties

        private MapContainer _Container;
        private String _HomeLocation;
        private String _TargetLocation;
        private Place _Home;
        private AlertInfo _CurrentAlert;
        private bool _IsLoading;
        private bool _WatingForAlert;
        private HydrantAlertInfos _HAlertInfos;
        

        public MapContainer Container
        {
            get { return _Container; }
            set { _Container = value; OnChanged(); }
        }      
        public String HomeLocation
        {
            get { return _HomeLocation; }
            set { _HomeLocation = value; OnChanged(); }
        } 
        public String TargetLocation
        {
            get { return _TargetLocation; }
            set { _TargetLocation = value; OnChanged(); }
        } 
        public Place Home
        {
            get { return _Home; }
            set { _Home = value; OnChanged(); }
        }     
        public AlertInfo CurrentAlert
        {
            get { return _CurrentAlert; }
            set { _CurrentAlert = value; OnChanged(); OnChanged("BTIsAlert"); OnChanged("BTIsNotAlert"); }
        }
        public bool IsLoading
        {
            get { return _IsLoading; }
            set { _IsLoading = value; OnChanged(); }
        }
        public HydrantAlertInfos HAlertInfos
        {
            get { return _HAlertInfos; }
            set { _HAlertInfos = value; OnChanged(); }
        }
 
  
        #endregion Properties

        #region ButtonVisbilities

        #region Alert
        public bool BTIsAlert
        {
            get
            {
                if (CurrentAlert != null)
                {
                    if (CurrentAlert.Status == AlertStatus.NoAlert)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }

        public bool BTIsNotAlert
        {
            get { return !BTIsAlert; }
        }

        #endregion Alert

        #region TownTools
        private bool _BTIsTowns;

        public bool BTIsTowns
        {
            get { return _BTIsTowns; }
            set { _BTIsTowns = value; OnChanged(); OnChanged("BTIsNotTowns");}
        }
        public bool BTIsNotTowns
        {
            get { return !BTIsTowns; }
        }

        #endregion

        #region ManuelRoute

        private bool _IsManuelRoutePopupOpen;
        private ManuelRoute _ManuelRouteProps;

        public bool IsManuelRoutePopupOpen
        {
            get { return _IsManuelRoutePopupOpen; }
            set { _IsManuelRoutePopupOpen = value; OnChanged(); }
        }

        public ManuelRoute ManuelRouteProps
        {
            get { return _ManuelRouteProps; }
            set { _ManuelRouteProps = value; OnChanged(); }
        }
        

        #endregion ManuelRoute

        #endregion ButtonVisibilities

        #region StackPanelVisibilities

        private bool _SPRouteVisible;
        public bool SPRouteVisible
        {
            get { return _SPRouteVisible; }
            set { _SPRouteVisible = value; OnChanged(); }
        }

        private bool _SPHydrantVisible;
        public bool SPHydrantVisible
        {
            get { return _SPHydrantVisible; }
            set { _SPHydrantVisible = value; OnChanged(); }
        }

        private bool _SPAlertVisible;
        public bool SPAlertVisible
        {
            get { return _SPAlertVisible; }
            set { _SPAlertVisible = value; OnChanged(); }
        }

        private bool _SPSettlementVisible;
        public bool SPSettlementVisible
        {
            get { return _SPSettlementVisible; }
            set { _SPSettlementVisible = value; OnChanged(); }
        }

        private bool _SPMapTools;

        public bool SPMapTools
        {
            get { return _SPMapTools; }
            set { _SPMapTools = value; OnChanged(); }
        }

        private bool _SPExternTools;

        public bool SPExternTools
        {
            get { return _SPExternTools; }
            set { _SPExternTools = value; OnChanged(); }
        }

        private bool _SPDocuement;

        public bool SPDocuement
        {
            get { return _SPDocuement; }
            set { _SPDocuement = value; OnChanged(); }
        }

        private bool _SPTowns;

        public bool SPTowns
        {
            get { return _SPTowns; }
            set { _SPTowns = value; OnChanged(); }
        }

        private bool _SPGPS;

        public bool SPGPS
        {
            get { return _SPGPS; }
            set { _SPGPS = value; OnChanged(); }
        }

        public bool IsGPSOn
        {
            get { return ConfigService.GPS; }
        }

        #endregion StackPanelVisibilities

        public MapViewModel()
        {
            
            SPAlertVisible = true;
            SPHydrantVisible = false;
            SPRouteVisible = false;
            SPSettlementVisible = false;
            SPMapTools = false;
            SPExternTools = false;
            SPDocuement = false;
            IsManuelRoutePopupOpen = false;

            Container = new MapContainer();
            ManuelRouteProps = new ManuelRoute();

            NewManuelRoute = new ActionCommand(OnNewManuelRouteExecute, OnNewManuelRouteCanExecute);
            CheckIfAlert = new ActionCommand(OnCheckIfAlertExecute, OnNewManuelRouteCanExecute);
            NewHydrantRoute = new ActionCommand(OnNewHydrantRouteExecute, OnNewHydrantRouteCanExecute);
            OpenAlarmFax = new ActionCommand(OnOpenAlarmFaxExecute, OnOpenAlarmFaxCanExecute);
            QuiteAlert = new ActionCommand(OnQuiteAlertExecute, OnQuiteAlertCanExecute);
            ChangeAlertLocation = new ActionCommand(OnChangeAlertLocationExecute, OnChangeAlertLocationCanExecute);

            SetStackPanel = new ActionCommand(OnSetStackPanelExecute, OnOnSetStackPanelCanExecute);
            AlertActions = new ActionCommand(OnAlertActionsExecute, OnAlertActionsCanExecute);
            HydrantActions = new ActionCommand(OnHydrantActionsExecute, OnHydrantActionsCanExecute);
            SettlementActions = new ActionCommand(OnSettlementActionsExecute, OnSettlementActionsCanExecute);
            RouteActions = new ActionCommand(OnRouteActionsExecute, OnRouteActionsCanExecute);
            MapToolsActions = new ActionCommand(OnMapToolsActionsExecute, OnMapToolsActionsCanExecute);
            ExternToolsActions = new ActionCommand(OnExternToolsActionsExecute, OnExternToolsActionsCanExecute);
            TeamActions = new ActionCommand(OnTeamActionsExecute, OnTeamActionsCanExecute);
            TownsActions = new ActionCommand(OnTownsActionsExecute, OnTownsActionsCanExecute);
            HydrantCalculateAction = new ActionCommand(OnHydrantCalculateActionExecute, OnHydrantCalculateActionCanExecute);
            GPSAction = new ActionCommand(OnGPSActionExecute, OnGPSActionCanExecute);
            ManuelRouteAction = new ActionCommand(OnManuelRouteActionExecute, OnManuelRouteActionCanExecute);
            CurrentAlert = new AlertInfo();
            SetHome();
            SaveAndLoadHydrants();
            SaveAndLoadSettlements();

        }

        #region Common

        private async void SaveAndLoadHydrants()
        {
            Container.Hydrants = new ObservableCollection<Hydrant>(await FileService.GetHydrantsXML());    
        }
        private async void SaveAndLoadSettlements()
        {
            Container.Settlements = new ObservableCollection<Settlement>(await FileService.GetSettlementsXML());
        }

        public void SetTappedHydrant(string s)
        {
            HAlertInfos = new HydrantAlertInfos();
            Container.TappedHydrant = Container.Hydrants.First(p => p.GUID.Equals(s));
        }
        public void SetTappedSettlement(string s)
        {
            Container.TappedSettlement = Container.Settlements.First(p => p.GUID.Equals(s));
        }

        private void NewAlert()
        {
            if (IsGPSOn)
            {
                Container.BTIsGPSOn = true;
            }
            NewRoute(CurrentAlert.Place1, CurrentAlert.Place2, RouteType.Alert);    
        }

        private async void NewRoute(String s1, String s2, RouteType rt)
        {
            if(!GlobalVariablesService.IsInternet())
            {
                NoInternet();
                return;
            }
            if (Container.RouteLocations != null)
            {
                Container.RouteLocations.Clear();
            }
            try
            {
                Location loc1 = await GoogleMapsService.GetGeoCodeFromAddress(s1);
                Location loc2 = await GoogleMapsService.GetGeoCodeFromAddress(s2);

                NewRoute(loc1, loc2, rt);
            }
            catch (RestException e)
            {
                ShowError(rt, e);
            }
        }

        private async void NewRoute(Location loc1, Location loc2, RouteType rt)
        {
            if (!GlobalVariablesService.IsInternet())
            {
                NoInternet();
                return;
            }
            if (Container.RouteLocations != null)
            {
                Container.RouteLocations.Clear();
            }
            try
            {
                RoutePropertiesResponse rpr = await BingMapsService.GetRouteLocations(loc1, loc2);
                if (rt == RouteType.Alert)
                {
                    CurrentAlert.AlertLocation = loc2;
                }
                else if (rt == RouteType.Manuel)
                {
                    ManuelRouteProps.TravelDistance = rpr.TravelDistance;
                    ManuelRouteProps.TravelDuration = rpr.TravelDuration;
                }
                MapPolyline myLine = new MapPolyline();
                foreach (Location l in rpr.RouteLocations)
                {
                    Container.RouteLocations.Add(l);
                    myLine.Locations.Add(l);
                }
                OnChanged("Locs");
                if (SetRouteView != null)
                    SetRouteView(myLine, rt);
                IsLoading = false;
            }
            catch (RestException e)
            {
                ShowError(rt, e);
            }
        }

        private async void ShowError(RouteType rt, RestException e)
        {
            MessageDialog md = null;
            if (e.RestExceptionType == RestExceptionType.NoLocationFound || e.RestExceptionType == RestExceptionType.NoRouteFound)
            {
                if ((int)rt == (int)RouteType.Alert)
                {
                    md = new MessageDialog("Es wurde ein Fehler bei der Suche gefunden, wollen sie den Einsatzort manuell eingeben?\n" + e.Message, "Fehler!");
                    UICommand uicYes = new UICommand("Ja");
                    uicYes.Invoked = YesBtnClick;
                    md.Commands.Add(uicYes);

                    UICommand uicNo = new UICommand("Nein");
                    uicNo.Invoked = NoBtnClick;
                    md.Commands.Add(uicNo);
                }
                else if ((int)rt == (int)RouteType.Manuel)
                {
                    md = new MessageDialog("Es wurde ein Fehler bei der manuellen Suche gefunden, bitte versuchen sie es erneut!\n" + e.Message, "Fehler!");
                    UICommand uicOk = new UICommand("OK");
                    md.Commands.Add(uicOk);
                }
                else if ((int)rt == (int)RouteType.Hydrant)
                {
                    md = new MessageDialog("Es wurde ein Fehler bei der Hydranten Suche gefunden\n" + e.Message, "Fehler!");
                    UICommand uicOk = new UICommand("OK");
                    md.Commands.Add(uicOk);
                }
                else if (((int)rt == (int)RouteType.Automatic))
                {
                    md = new MessageDialog("Es wurde ein Fehler bei der automatischen Suche gefunden\n" + e.Message, "Fehler!");
                    UICommand uicOk = new UICommand("OK");
                    md.Commands.Add(uicOk);

                }
            }
            else
            {
                md = new MessageDialog("Es wurde ein Fehler bei der Suche gefunden\n" + e.Message, "Fehler!");
                UICommand uicOk = new UICommand("OK");
                md.Commands.Add(uicOk);

            }

            try
            {
                await Task.Run(() => md.ShowAsync());
            }
            catch(UnauthorizedAccessException ex) { }
            IsLoading = false;
        }


        private void NoBtnClick(IUICommand command)
        {
            IsLoading = false;
        }

        private async void YesBtnClick(IUICommand command)
        {
            InputMessageDialog dlg = new InputMessageDialog(CurrentAlert.Place1, CurrentAlert.Place2, CurrentAlert.OriginalAddress);
            bool result = await dlg.ShowAsync();
            if (result != false)
            {
                var userInput = dlg.TextBox.Text.Split('&');
                CurrentAlert.Place1 = userInput[0];
                CurrentAlert.Place2 = userInput[1];
                IsLoading=true;
                NewRoute(CurrentAlert.Place1, CurrentAlert.Place2, RouteType.Alert);
            }
        }

        private async Task<Place> GetCoordFromLocationAsync(String name, bool isStartUp=false)
        {
            try
            {
                Location loc = await GoogleMapsService.GetGeoCodeFromAddress(name);
                return new Place(name, loc.Longitude, loc.Latitude);

            }
            catch (RestException e)
            {
                if (!isStartUp)
                {
                    ShowError(RouteType.Automatic, e);
                }
                return null;
            }
        }

        private async void Sleep(int seconds)
        {
            System.Threading.Tasks.Task.Delay(seconds*1000).Wait();
        }

        private async void SetHome()
        {
            if (!GlobalVariablesService.IsInternet())
            {
                NoInternet();
                return;
            }

            Home = await GetCoordFromLocationAsync(ConfigService.Home, true);
            if (SetRouteViewLocation != null && Home != null)
            {
                SetRouteViewLocation(new Location(Home.Latitude, Home.Longitude));
            }
            else
            {
                MessageDialog md = new MessageDialog("Es wurde ein Fehler bei dem Startvorgang gefunden, soll es erneut versucht werden?\n" + "");
                UICommand uicYes = new UICommand("Ja");
                uicYes.Invoked = InitBtnYes;
                md.Commands.Add(uicYes);

                UICommand uicNo = new UICommand("Nein");
                uicNo.Invoked = InitBtnNo;
                md.Commands.Add(uicNo);
                await md.ShowAsync();
                IsLoading = false;
            }
        }

        private void InitBtnNo(IUICommand command)
        {
            App.Current.Exit();
        }

        private void InitBtnYes(IUICommand command)
        {
            SetHome();
        }

        private async void SetNewAlertLocation()
        {
            InputMessageDialog dlg = new InputMessageDialog(CurrentAlert.Place1, CurrentAlert.Place2, CurrentAlert.OriginalAddress);
            bool result = await dlg.ShowAsync();
            if (result != false)
            {
                var userInput = dlg.TextBox.Text.Split('&');
                CurrentAlert.Place1 = userInput[0];
                CurrentAlert.Place2 = userInput[1];
                IsLoading = true;
                NewRoute(CurrentAlert.Place1, CurrentAlert.Place2, RouteType.Alert);
            }
            
        }

        private string Filter(string s)
        {
            s=s.Replace("&", "und");

            //BMA
            string bmaString = System.Text.RegularExpressions.Regex.Match(s, @"\d+").Value;
            if (System.Text.RegularExpressions.Regex.Matches(s, bmaString).Count >= 2)
            {
                string searchstring = bmaString;
                int Start = s.IndexOf(searchstring, 0);
                int End = s.IndexOf(searchstring, Start + 1);
                s=s.Substring(Start+searchstring.Length+1, End-(searchstring.Length+1));
            }
            return s;
        }

        private async void CheckIfAlertAsync()
        {
            if(!GlobalVariablesService.IsInternet())
            {
                NoInternet();
                return;
            }
            CurrentAlert = await EMailService.CheckIfAlert();
            if (CurrentAlert.AlertDateTime.AddHours(2) < DateTime.Now)
            {
                MessageDialog md = new MessageDialog(String.Format("Es gibt keinen neuen Einsatz, letzter Einsatz ist vom {0}.{1}.{2} um {3}:{4} !\n\n Soll auf einen neuen Einsatz gewartet werden?",
                    CurrentAlert.AlertDateTime.Day, CurrentAlert.AlertDateTime.Month, CurrentAlert.AlertDateTime.Year, CurrentAlert.AlertDateTime.Hour, CurrentAlert.AlertDateTime.Minute), 
                    "Kein akuteller Einsatz vorhanden!");
                UICommand uic = new UICommand("Ja, auf neuen Einsatz warten...");
                uic.Invoked = WaitForNewAlert;
                md.Commands.Add(uic);
                uic = new UICommand("Nein, alten Einsatz laden.");
                uic.Invoked = LoadOldAlert;
                md.Commands.Add(uic);
                await md.ShowAsync();
            }
            else
            {
                if (CurrentAlert.Place1 == null)
                    CurrentAlert.Place1 = Home.Name;
                CurrentAlert.Place2 = Filter(CurrentAlert.Place2);
                if (CurrentAlert != null)
                    NewAlert();
            }
        }

        private void LoadOldAlert(IUICommand command)
        {
            CurrentAlert.Status = AlertStatus.OldAlert;
            if (CurrentAlert.Place1 == null)
                CurrentAlert.Place1 = Home.Name;
            CurrentAlert.Place2 = Filter(CurrentAlert.Place2);
            if (CurrentAlert != null)
                NewAlert();
        }
        private async void WaitForNewAlert(IUICommand command)
        {
            _WatingForAlert = true;
            MessageDialog md = new MessageDialog("...", "Warten auf Einsatzdaten");
            UICommand uic = new UICommand("Abbrechen...");
            uic.Invoked = QuitSearching;
            md.Commands.Add(uic);
            var asyncOperation = md.ShowAsync();

            while (_WatingForAlert)
            {
                AlertInfo newAlert = await EMailService.CheckIfAlert();
                if (newAlert.AlertDateTime.AddHours(2) > DateTime.Now)
                {
                    _WatingForAlert = false;
                    if (newAlert.Place1 == null)
                        newAlert.Place1 = Home.Name;
                    newAlert.Place2 = Filter(newAlert.Place2);
                    if (newAlert != null)
                    {
                        asyncOperation.Cancel();
                        CurrentAlert = newAlert;
                        NewAlert();
                    }
                }
                if (_WatingForAlert)
                    Sleep(5);             
            }
        }
        private void QuitSearching(IUICommand command)
        {
            _WatingForAlert = false;
            IsLoading = false;
            CurrentAlert = new AlertInfo();
            OnChanged("BTIsNotAlert");
            OnChanged("BTIsAlert");
        }

        public void MapTapped(Location l)
        {
            if(Container.BTIsArea)
            {
                if (Container.BTIsPolygonPainting)
                {
                    Container.Polygons[Container.Polygons.Count - 1].PolyLine.Locations.Add(l);
                    Container.RaisePropertyChanged(RaisePropertyChangedTyp.Polygons);
                    if (SetPolygons != null)
                        SetPolygons(Container.GetAllPolygons());
                    if (RefreshMap != null)
                    {
                        RefreshMap(null, null);
                    }
                }
                else if (Container.BTIsPolylinePainting)
                {
                    Container.Polylines[Container.Polylines.Count - 1].PolyLine.Locations.Add(l);
                    Container.RaisePropertyChanged(RaisePropertyChangedTyp.Polylines);
                    if (SetPolylines != null)
                        SetPolylines(Container.GetAllPolylines());
                    if (RefreshMap != null)
                    {
                        RefreshMap(null, null);
                    }
                }
                else if (Container.BTIsCirclePainting)
                {
                    CustomMapCircle circle = Container.Circles[Container.Circles.Count - 1];
                    if (circle.Loc1 == null)
                    {
                        circle.Loc1 = new IDLocation("C:"+(Container.Circles.Count-1).ToString() + ":1", l);
                    }
                    else
                    {
                        circle.Loc2 = new IDLocation("C:" + (Container.Circles.Count-1).ToString() + ":2", l);
                        Container.BTIsCirclePainting = false;
                        IEnumerable<Location> locs = Container.CalcCircle(circle);
                        foreach (Location loc in locs)
                        {
                            circle.Circle.Locations.Add(loc);
                        }

                        Container.RaisePropertyChanged(RaisePropertyChangedTyp.Circles);
                        if (SetCircles != null)
                            SetCircles(Container.GetAllCircles());
                    }
                    if (RefreshMap != null)
                    {
                        RefreshMap(null, null);
                    }
                }
            }

        }

        private async void NoInternet()
        {
            MessageDialog md = new MessageDialog("Es konnte keine Verbindung zum Internet hergestellt werden.");
            UICommand uicOk = new UICommand("OK");
            await md.ShowAsync();
            IsLoading = false;
            return;
        }

        private string GetHomeIfEmpty(string s)
        {
            if(s==null)
            {
                if (BTIsAlert)
                {
                    return CurrentAlert.Place1;
                }
                else
                {
                    return Home.Name;
                }
            }
            else if (s!="")
            {
                return s;
            }
            else if (BTIsAlert)
            {
                return CurrentAlert.Place2;
            }
            else
            {
                return Home.Name;
            }
        }
    
        #endregion Common

        #region CommandMethods

        private bool OnManuelRouteActionCanExecute(object arg)
        {
            if (arg != null)
            {
                return Convert.ToBoolean(arg);
            }
            return false;
        }

        private void OnManuelRouteActionExecute(object obj)
        {
            IsLoading = true;
            if (string.IsNullOrWhiteSpace(ManuelRouteProps.Location1))
            {
                NewRoute(GetHomeIfEmpty(HomeLocation), ManuelRouteProps.Location2, RouteType.Manuel);
            }
            else
            {
                NewRoute(ManuelRouteProps.Location1, ManuelRouteProps.Location2, RouteType.Manuel);
            }
        }

        private bool OnGPSActionCanExecute(object arg)
        {
            return true;
        }

        private void OnGPSActionExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "TurnOnGPS": Container.BTIsGPSOn = true; break;
                case "TurnOffGPS": Container.BTIsGPSOn = false; break;
            }
        }

        private bool OnOnSetStackPanelCanExecute(object arg)
        {
            return true;
        }

        private void OnSetStackPanelExecute(object obj)
        {
            SPAlertVisible = false;
            SPHydrantVisible = false;
            SPRouteVisible = false;
            SPSettlementVisible = false;
            SPMapTools = false;
            SPExternTools = false;
            SPDocuement = false;
            SPTowns = false;
            SPGPS = false;

            string newSP = obj.ToString();
            switch(newSP)
            {
                case "Route": SPRouteVisible = true;  break;
                case "Alert": SPAlertVisible = true; break;
                case "Hydrant": SPHydrantVisible = true; break;
                case "Settlement": SPSettlementVisible = true; break;
                case "MapTools": SPMapTools = true; break;
                case "ExternTools": SPExternTools = true; break;
                case "Document": SPDocuement = true; break;
                case "Towns": SPTowns = true; break;
                case "GPS": SPGPS = true; break;
            }
            OnChanged("IsGPSOn");
        }

        private bool OnAlertActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnAlertActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "CheckIfAlert": IsLoading = true; CheckIfAlertAsync(); break;
                case "OpenAlarmFax": FileService.OpenAlarmFax(); break;
                case "ChangeAlertLocation": SetNewAlertLocation();break;
                case "QuiteAlert": CurrentAlert = new AlertInfo();
                    if (RemoveRouteFromMap != null)
                        RemoveRouteFromMap(RouteType.Alert);
                    if (SetRouteViewLocation != null)
                        SetRouteViewLocation(new Location(Home.Latitude, Home.Longitude)); 
                    if (IsGPSOn)
                    {
                        Container.BTIsGPSOn = false;
                    }
            break;
            }
        }

        private bool OnHydrantActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnHydrantActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "ShowHydrants": Container.BTIsHydrants=true; break;
                case "SchowNotHydrants": Container.BTIsHydrants=false; break;
            }
        }

        private bool OnSettlementActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnSettlementActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "ShowSettlements": Container.BTIsSettlements = true; break;
                case "ShowNotSettlements": Container.BTIsSettlements = false; break;
            }
        }

        private bool OnRouteActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnRouteActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                //case "New": ManuellRoute(); break;
                case "Show": IsManuelRoutePopupOpen = true; break;
                case "Hide": IsManuelRoutePopupOpen = false; break;
                case "Remove": ManuelRouteProps = new ManuelRoute(); break;
            }
        }

        private bool OnMapToolsActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnMapToolsActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "ShowAreas": Container.BTIsArea = true; SetPolygons(Container.GetAllPolygons()); SetPolylines(Container.GetAllPolylines()); SetCircles(Container.GetAllCircles()); Container.RaisePropertyChanged(RaisePropertyChangedTyp.All); if (RefreshMap != null) { RefreshMap(null, null); } break;
                case "ShowNotAreas": Container.BTIsArea = false; SetPolygons(Container.GetAllPolygons()); SetPolylines(Container.GetAllPolylines()); SetCircles(Container.GetAllCircles()); Container.RaisePropertyChanged(RaisePropertyChangedTyp.All); Container.BTIsPolygonPainting = false; Container.BTIsPolylinePainting = false; Container.BTIsCirclePainting = false; break;
                case "NewPolygon": if (Container.BTIsArea == true) { NewPolygon(); Container.BTIsPolygonPainting = true; Container.BTIsPolylinePainting = false; Container.BTIsCirclePainting = false; } break;
                case "StopNewPolygon": Container.BTIsPolygonPainting = false; Container.CheckStopPainting(CheckStopPaintingTyp.Polygons); break;
                case "NewPolyline": if (Container.BTIsArea == true) { NewPolyline(); Container.BTIsPolylinePainting = true; Container.BTIsPolygonPainting = false; Container.BTIsCirclePainting = false; } break;
                case "StopNewPolyline": Container.BTIsPolylinePainting = false; Container.CheckStopPainting(CheckStopPaintingTyp.Polylines); break;
                case "NewCircle": if (Container.BTIsArea == true) { NewCircle(); Container.BTIsPolylinePainting = false; Container.BTIsPolygonPainting = false; Container.BTIsCirclePainting = true; } break;
                case "StopNewCircle": Container.BTIsCirclePainting = false; Container.CheckStopPainting(CheckStopPaintingTyp.Circles); break;
                case "DeleteAll": Container.Polygons.Clear(); SetPolygons(Container.GetAllPolygons()); Container.BTIsPolygonPainting = false;  
                                  Container.Polylines.Clear(); SetPolylines(Container.GetAllPolylines()); Container.BTIsPolylinePainting=false;
                                  Container.Circles.Clear(); SetCircles(Container.GetAllCircles()); Container.BTIsCirclePainting=false;
                                  Container.RaisePropertyChanged(RaisePropertyChangedTyp.All);break;
            }
        }

        private bool OnExternToolsActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnExternToolsActionsExecute(object obj)
        {
            string action = obj.ToString().Split('#')[0];
            string param = obj.ToString().Split('#')[1];
            switch (action)
            {
                case "Web":
                    switch (param)
                    {
                        case "Rettungskarten": LaunchWeb("https://i122.i122.at/api3/users/login"); break;
                    }
                    break;
            }
        }
        
        private bool OnTeamActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnTeamActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {

            }
        }

        private bool OnTownsActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnTownsActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "ShowTowns": BTIsTowns = true; ShowTowns(); break;
                case "ShowNotTowns": BTIsTowns = false; break;
            }
        } 



        bool OnNewManuelRouteCanExecute(object parameter)
        {
            return true;
        }

        void OnNewManuelRouteExecute(object parameter)
        {
            IsLoading = true;
            NewRoute(GetHomeIfEmpty(HomeLocation), TargetLocation, RouteType.Manuel);
        }
        bool OnCheckIfAlertCanExecute(object parameter)
        {
            return true;
        }

        void OnCheckIfAlertExecute(object parameter)
        {
            IsLoading = true;
            CheckIfAlertAsync();
        }

        bool OnOpenAlarmFaxCanExecute(object parameter)
        {
            return true;
        }

        void OnOpenAlarmFaxExecute(object parameter)
        {
            FileService.OpenAlarmFax();
        }

        bool OnQuiteAlertCanExecute(object parameter)
        {
            return true;
        }

        void OnQuiteAlertExecute(object parameter)
        {
            CurrentAlert = new AlertInfo();
            if (RemoveRouteFromMap != null)
                RemoveRouteFromMap(RouteType.Alert);
            if (SetRouteViewLocation != null)
                SetRouteViewLocation(new Location(Home.Latitude, Home.Longitude));
        }


        void OnNewHydrantRouteExecute(object parameter)
        {
            IsLoading = true;
            if (CurrentAlert.Place2 != null)
            {
                NewRoute(CurrentAlert.Place2, Container.TappedHydrant.Latitude + " " + Container.TappedHydrant.Longitude, RouteType.Hydrant);
                return;
            }
            if (TargetLocation != null)
            {
                if (TargetLocation != "")
                {
                    NewRoute(TargetLocation, Container.TappedHydrant.Latitude + " " + Container.TappedHydrant.Longitude, RouteType.Hydrant);
                    return;
                }
            }
            if (Home != null)
                NewRoute(new Location(Home.Latitude, Home.Longitude), new Location(Container.TappedHydrant.Latitude, Container.TappedHydrant.Longitude), RouteType.Hydrant);
        }

        bool OnNewHydrantRouteCanExecute(object parameter)
        {
            return true;
        }

        private bool OnChangeAlertLocationCanExecute(object arg)
        {
            return true;
        }

        private void OnChangeAlertLocationExecute(object obj)
        {
            SetNewAlertLocation();
        }

        private bool OnChangeAreasStateCanExecute(object arg)
        {
            return true;
        }

        private bool OnHydrantCalculateActionCanExecute(object arg)
        {
            return true;
        }

        private async void OnHydrantCalculateActionExecute(object obj)
        {
            IsLoading = true;
            if (Container.TappedHydrant != null && CurrentAlert!=null && CurrentAlert.Status!= AlertStatus.NoAlert )
            {
                if (Container.TappedHydrant.Latitude != 0 && CurrentAlert.AlertLocation != null)
                {
                    MapPolyline mp = new MapPolyline();
                    mp.Locations.Add(new Location(Container.TappedHydrant.Latitude, Container.TappedHydrant.Longitude));
                    mp.Locations.Add(CurrentAlert.AlertLocation);
                    HAlertInfos.AirDistanceToAlert = Container.CalculateDistance(mp);

                    try
                    {
                         HAlertInfos.WayDistanceToAlert = await BingMapsService.GetRouteDistance(new Location(Container.TappedHydrant.Latitude, Container.TappedHydrant.Longitude), CurrentAlert.AlertLocation);                
                    }
                    catch
                    { }
                }
            }
            IsLoading = false;
        }

        #endregion CommandsMethods

        #region CommandHelperMethods
        private async void ManuellRoute()
        {
            InputMessageDialog dlg = new InputMessageDialog("", "");
            bool result = await dlg.ShowAsync();
            if (result != false)
            {
                var userInput = dlg.TextBox.Text.Split('&');
                HomeLocation = userInput[0];
                TargetLocation = userInput[1];
                IsLoading = true;

                NewRoute(GetHomeIfEmpty(HomeLocation), TargetLocation, RouteType.Manuel);
            }
        }

        private async void NewPolygon()
        {
            ColorPicker cp = new ColorPicker();
            Windows.UI.Color result = await cp.ShowAsync();
            CustomMapPolygon mp = new CustomMapPolygon();
            mp.PolyLine = new MapPolygon { FillColor = result };
            Container.Polygons.Add(mp);
        }

        private async void NewPolyline()
        {
            ColorPicker cp = new ColorPicker();
            Windows.UI.Color result = await cp.ShowAsync();
            CustomMapPolyline mp = new CustomMapPolyline();
            mp.PolyLine = new MapPolyline { Color = result, Width = 5 };
            Container.Polylines.Add(mp);
        }

        private async void NewCircle()
        {
            ColorPicker cp = new ColorPicker();
            Windows.UI.Color result = await cp.ShowAsync();
            CustomMapCircle mp = new CustomMapCircle();
            mp.Circle = new MapPolygon { FillColor = result };
            Container.Circles.Add(mp);
        }

        private async void LaunchWeb(string name)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri(name));
        }

        private async void ShowTowns()
        {
            if (TownService.IsLoaded == false)
            {
                if (SetTowns != null)
                {
                    IsLoading = true;

                    bool load = await ShowTownsAsync();

                    IsLoading = false;
                }
            }
        }

        private async Task<bool> ShowTownsAsync()
        {
            
            List<MapPolyline> lines = new List<MapPolyline>();
            if (TownService.BorderPoints == null || TownService.Towns == null || TownService.Ways == null)
            {
                bool load = await TownService.LoadTownService();
            }
            foreach (Way way in TownService.Ways.Values)
            {
                MapPolyline mp = new MapPolyline();
                mp.Color = Windows.UI.Colors.Black;
                mp.Width = 3;
                foreach (double Refid in way.RefValues)
                {
                    BorderPoint point = TownService.BorderPoints[Refid];
                    mp.Locations.Add(new Location(point.Lat, point.Lon));
                }
                lines.Add(mp);
            }
            SetTowns(lines);
            TownService.IsLoaded = true;
            return true;
        }

        #endregion CommandHelperMethods

        #region DragAndDrop
       
        public void SetArePinNew(int count1, int count2, Location location)
        {
            if (Container.BTIsPolygonPainting == false)
            {
                Container.Polygons[count1].PolyLine.Locations[count2] = location;
                Container.RaisePropertyChanged(RaisePropertyChangedTyp.Polygons);
                if (SetPolygons != null)
                    SetPolygons(Container.GetAllPolygons());
                if (RefreshMap != null)
                {
                    RefreshMap(null, null);
                }
            }
        }

        public void SetLinePinNew(int count1, int count2, Location location)
        {
            if (Container.BTIsPolylinePainting == false)
            {
                Container.Polylines[count1].PolyLine.Locations[count2] = location;
                Container.RaisePropertyChanged(RaisePropertyChangedTyp.Polylines);
                if (SetPolylines != null)
                    SetPolylines(Container.GetAllPolylines());
                if (RefreshMap != null)
                {
                    RefreshMap(null, null);
                }
            }
        }

        public void SetCirclePinNew(int count1, int count2, Location location)
        {
            if (Container.BTIsCirclePainting == false)
            {
                if (count2 == 1)
                {
                    double difLat = location.Latitude - Container.Circles[count1].Loc1.Locs.Latitude;
                    double difLon = location.Longitude - Container.Circles[count1].Loc1.Locs.Longitude;
                    Container.Circles[count1].Loc2.Locs.Latitude += difLat;
                    Container.Circles[count1].Loc2.Locs.Longitude += difLon;

                    Container.Circles[count1].Loc1.Locs = location;
                }
                else if(count2==2)
                {
                    Container.Circles[count1].Loc2.Locs = location;
                }
                IEnumerable<Location> locs = Container.CalcCircle(Container.Circles[count1]);
                Container.Circles[count1].Circle.Locations.Clear();
                foreach (Location loc in locs)
                    Container.Circles[count1].Circle.Locations.Add(loc);
                Container.RaisePropertyChanged(RaisePropertyChangedTyp.Circles);
                if (SetCircles != null)
                    SetCircles(Container.GetAllCircles());
                if (RefreshMap != null)
                {
                    RefreshMap(null, null);
                }
            }
        }


        #endregion DragAndDrop
    }
}
