﻿using Einsatz_Karte.Model;
using Einsatz_Karte.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Einsatz_Karte.ViewModel.Settings
{
    public class ConfigViewModel : ConfigBaseViewModel
    {
        #region Constructors
        public ConfigViewModel()
        {
            Items = new ObservableCollection<object>();
            List<string> sortetKeys = ConfigService.Config.Keys.ToList();
            foreach(string key in GetAllConfigParameter())
            {
                if (!sortetKeys.Contains(key))
                {
                    sortetKeys.Add(key);
                }
            }
            sortetKeys.Sort();
            foreach (string key in sortetKeys)
            {
                if (ConfigService.Config.Keys.Contains(key))
                {
                    Items.Add(new ConfigItem(key, ConfigService.Config[key]));
                }
                else
                {
                    Items.Add(new ConfigItem(key, ""));
                }
            }

        }
        #endregion Constructors

        #region Override
        public override void Action_Save()
        {
            ConfigService.SaveConfig(Items.Cast<ConfigItem>().ToList());
            ShowSaveSuccess();
        }


        protected override string CheckItemForSave(object obj)
        {
            ConfigItem item = (ConfigItem)obj;

            //if (item.ConfigName == null || item.ConfigName == "")
            //{
            //    return "Der Key Parameter darf nicht leer sein!";
            //}
            //else if (item.ConfigName == null || item.ConfigName == "")
            //{
            //    return "Der Value Parameter darf nicht leer sein!";
            //}
            //else if (IsNewEntry && Items.Cast<ConfigItem>().Where(p => p.ConfigName == item.ConfigName).Count() > 0)
            //{
            //    return "Der Key Parameter darf nicht doppelt vorkommen!";
            //}
            return "";
        }
        #endregion Override

        #region Common

        private List<String> GetAllConfigParameter()
        {
            List<string> cp = new List<string>();

            cp.Add("BingMapsKey");
            cp.Add("EmailAddress");
            cp.Add("EmailPassword");
            cp.Add("EmailsForReport");
            cp.Add("GoogleMapsKey");
            cp.Add("GPS");
            cp.Add("Home");
            cp.Add("ImapAddress");
            cp.Add("SmtpAddress");

            return cp;
        }

        #endregion Common
    }
}
