﻿using Einsatz_Karte.CustomControls;
using Einsatz_Karte.Model;
using Einsatz_Karte.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Einsatz_Karte.ViewModel.Settings
{
    class HydrantsConfigViewModel : ConfigBaseViewModel
    {
        #region Declarations
        public event EventHandler LocationChanged;
        public ICommand CalculateCoords { get; private set; }
        public ICommand OpenImage { get; private set; }
        public ICommand ChangeImage { get; private set; }
        #endregion Declarations

        #region Constructors
        public HydrantsConfigViewModel()
        {
            Items = new ObservableCollection<object>();
            CalculateCoords = new ActionCommand(OnCalculateCoordsExecute, OnCalculateCoordsCanExecute);
            OpenImage = new ActionCommand(OnOpenImageExecute, OnOpenImageCanExecute);
            ChangeImage = new ActionCommand(OnChangeImageExecute, OnChangeImageCanExecute);
            LoadData();
        }
        #endregion Constructors

        #region Commands

        #region Execute
        private async void OnChangeImageExecute(object obj)
        {
            Hydrant h = (Hydrant)SelectedItem;
            if (h != null)
            {
                await FileService.SaveNewHydrantImage(h);
            }
        }

        private async void OnOpenImageExecute(object obj)
        {
            Hydrant h = (Hydrant)SelectedItem;
            if (h != null)
            {
                if (h.ImageUrl != null && h.ImageUrl != "")
                {
                    await FileService.OpenFile(FileService.LocalFolderUrl + h.ImageUrl);
                }
                else
                {
                    await FileService.SaveNewHydrantImage(h, true);
                    OnOpenImageExecute(h);
                }
            }
        }

        private async void OnCalculateCoordsExecute(object obj)
        {
            Hydrant item = (Hydrant)SelectedItem;
            try
            {
                Bing.Maps.Location loc = await GoogleMapsService.GetGeoCodeFromAddress(item.Address);
                item.Latitude = loc.Latitude;
                item.Longitude = loc.Longitude;
                if (LocationChanged != null)
                {
                    LocationChanged(this, new EventArgs());
                }
            }
            catch (Exception e)
            {
                ShowSaveError("Die Adresse \"" + item.Address + "\" konnte nicht aufgelöst werden!\n" + e.Message + " \n");
                return;
            }
        }

        #endregion Execute

        #region CanExecute
        private bool OnChangeImageCanExecute(object arg)
        {
            return true;
        }

        private bool OnOpenImageCanExecute(object arg)
        {
            return true;
        }

        private bool OnCalculateCoordsCanExecute(object arg)
        {
            return true;
        }
        #endregion CanExecute

        #endregion Commands

        #region Methods
        private async void LoadData()
        {
            IEnumerable<Hydrant> items = await FileService.GetHydrantsXML();
            foreach (Hydrant h in items.OrderBy(p => p.Address))
            {
                Items.Add(h);
            }
        }
        #endregion Methods

        #region Override
        public async override void Action_Save()
        {
            await FileService.DeleteUnnecessaryHydrantImages(Items.Cast<Hydrant>().ToList());
            await FileService.SaveHaydrantsXML(Items.Cast<Hydrant>().ToList());
            ShowSaveSuccess();
        }

        protected override string CheckItemForSave(object obj)
        {
            Hydrant item = (Hydrant)obj;
            if (item.ImageUrl == null || item.ImageUrl == "")
            {
                FileService.SaveNewHydrantImage(item, true);
            }
            if (item.Address == "" || item.Address == null)
            {
                return "Die Adresse darf nicht  leer sein!\n";
            }
            else if (IsNewEntry && Items.Cast<Hydrant>().Where(p => p.Address == item.Address).Count() > 0)
            {
                return "Die Adresse \"" + item.Address + "\" kommt doppelt vor!\n";
            }
            else if (item.Latitude <= 0 || item.Longitude <= 0)
            {
                return "Es müssen vorher die Koordinaten berechnet werden!\n";
            }
            return "";
        }
        #endregion Override

    }
}
