﻿using Einsatz_Karte.Model;
using Einsatz_Karte.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;

namespace Einsatz_Karte.ViewModel.Settings
{
    class SettlementsConfigViewModel : ConfigBaseViewModel
    {
        #region Declarations
        public event EventHandler LocationChanged;
        public ICommand CalculateCoords { get; private set; }
        public ICommand OpenImage { get; private set; }
        public ICommand ChangeImage { get; private set; }
        public ICommand AddInfo { get; private set; }
        public ICommand RemoveInfo { get; private set; }
        #endregion Declarations

        #region Constructors
        public SettlementsConfigViewModel()
        {
            Items = new ObservableCollection<object>();
            CalculateCoords = new ActionCommand(OnCalculateCoordsExecute, OnCalculateCoordsCanExecute);
            OpenImage = new ActionCommand(OnOpenImageExecute, OnOpenImageCanExecute);
            ChangeImage = new ActionCommand(OnChangeImageExecute, OnChangeImageCanExecute);
            AddInfo = new ActionCommand(OnAddInfoExecute, OnAddInfoCanExecute);
            RemoveInfo = new ActionCommand(OnRemoveInfoExecute, OnRemoveInfoCanExecute);
            LoadData();
       
        }
        #endregion Constructors

        #region Commands

        #region Execute
        private void OnRemoveInfoExecute(object obj)
        {
            if (obj != null)
            {
                ((Settlement)SelectedItem).Info.Remove((SettlementInfo)obj);
            }
        }
        private void OnAddInfoExecute(object obj)
        {
            if (((Settlement)SelectedItem).Info == null)
            {
                ((Settlement)SelectedItem).Info = new ObservableCollection<SettlementInfo>();
            }
            ((Settlement)SelectedItem).Info.Add(new SettlementInfo { Title = "NEU", Value = "NEU" });
        }
        private async void OnChangeImageExecute(object obj)
        {
            Settlement item = (Settlement)SelectedItem;
            if (item != null)
            {
                await FileService.SaveNewSettlementImage(item);
            }
        }
        private async void OnOpenImageExecute(object obj)
        {
            Settlement item = (Settlement)SelectedItem;
            if (item != null)
            {
                if (item.ImageUrl != null && item.ImageUrl != "")
                {
                    await FileService.OpenFile(FileService.LocalFolderUrl + item.ImageUrl);
                }
                else
                {
                    await FileService.SaveNewSettlementImage(item, true);
                    OnOpenImageExecute(item);
                }
            }
        }

        private async void OnCalculateCoordsExecute(object obj)
        {
            Settlement item = (Settlement)SelectedItem;
            try
            {
                Bing.Maps.Location loc = await GoogleMapsService.GetGeoCodeFromAddress(item.Address);
                item.Latitude = loc.Latitude;
                item.Longitude = loc.Longitude;
                if (LocationChanged != null)
                {
                    LocationChanged(this, new EventArgs());
                }
            }
            catch (Exception e)
            {
                ShowSaveError("Die Adresse \"" + item.Address + "\" konnte nicht aufgelöst werden!\n" + e.Message + " \n");
                return;
            }
        }
        #endregion Execute

        #region CanExecute
        private bool OnRemoveInfoCanExecute(object arg)
        {
            return true;
        }
        private bool OnAddInfoCanExecute(object arg)
        {
            return true;
        }
        private bool OnChangeImageCanExecute(object arg)
        {
            return true;
        }
        private bool OnOpenImageCanExecute(object arg)
        {
            return true;
        }
        private bool OnCalculateCoordsCanExecute(object arg)
        {
            return true;
        }
        #endregion CanExecute

        #endregion Commands

        #region Methods
        private async void LoadData()
        {
            IEnumerable<Settlement> items = await FileService.GetSettlementsXML();
            foreach (Settlement s in items.OrderBy(p => p.Name))
            {
                Items.Add(s);
            }
        }
        #endregion Methods

        #region Override
        public async override void Action_Save()
        {
            await FileService.DeleteUnnecessarySettlementsImages(Items.Cast<Settlement>().ToList());
            await FileService.SaveSettlementsXML(Items.Cast<Settlement>().ToList());
            ShowSaveSuccess();
        }


        protected override string CheckItemForSave(object obj)
        {
            Settlement item = (Settlement)obj;
            if (item.ImageUrl == null || item.ImageUrl == "")
            {
                FileService.SaveNewSettlementImage(item, true);
            }
            if (item.Address == "" || item.Address == null)
            {
                return "Die Adresse darf nicht  leer sein!\n";
            }
            else if (IsNewEntry && Items.Cast<Settlement>().Where(p => p.Address == item.Address).Count() > 0)
            {
                return "Die Adresse \"" + item.Address + "\" kommt doppelt vor!\n";
            }
            else if (item.Latitude <= 0 || item.Longitude <= 0)
            {
                return "Es müssen vorher die Koordinaten berechnet werden!\n";
            }
            return "";
        }
        #endregion Override        
    }
}
