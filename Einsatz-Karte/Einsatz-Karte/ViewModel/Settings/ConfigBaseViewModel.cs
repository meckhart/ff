﻿using Einsatz_Karte.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace Einsatz_Karte.ViewModel.Settings
{
    public abstract class ConfigBaseViewModel : BaseViewModel
    {
        #region Declarations
        private ObservableCollection<object> _Items;
        public ObservableCollection<object> Items
        {
            get { return _Items; }
            set { _Items = value; }
        }

        private Object _SelectedItem;
        public Object SelectedItem
        {
            get { return _SelectedItem; }
            set { _SelectedItem = value; OnChanged();}
        }

        private bool _IsPopupOpen;

        public bool IsPopupOpen
        {
            get { return _IsPopupOpen; }
            set { _IsPopupOpen = value; OnChanged(); }
        }

        private bool _IsNewEntry;

        public bool IsNewEntry
        {
            get { return _IsNewEntry; }
            set { _IsNewEntry = value; OnChanged(); OnChanged("IsNotNewEntry"); }
        }

        private bool _ItemsChanged;

        public bool ItemsChanged
        {
            get { return _ItemsChanged; }
            set { _ItemsChanged = value; OnChanged(); }
        }
        
             
        public ICommand New { get; private set; }
        public ICommand Remove { get; private set; }
        public ICommand Save { get; private set; }
        public ICommand SaveItem { get; private set; }
        public ICommand Edit { get; private set; }

        #endregion Declarations

        #region Constructors
        public ConfigBaseViewModel()
        {
            New = new ActionCommand(OnNewExecute, OnNewCanExecute);
            Remove = new ActionCommand(OnRemoveExecute, OnRemoveCanExecute);
            Save = new ActionCommand(OnSaveExecute, OnSaveCanExecute);
            Edit = new ActionCommand(OnEditExecute, OnEditCanExecute);
            SaveItem = new ActionCommand(OnSaveItemExecute, OnSaveItemCanExecute);
            IsPopupOpen = false;
            ItemsChanged = false;
        }
        #endregion Constructors

        #region Commands

        #region Execute

        private void OnSaveItemExecute(object obj)
        {
            if (obj != null && obj.ToString() == "Abort")
            {
                IsPopupOpen = false;
                return;
            }

            string error = CheckItemForSave(SelectedItem);
            if (error == "")
            {
                if (IsNewEntry)
                {
                    Items.Add(SelectedItem);
                    ItemsChanged = true;
                }
                IsPopupOpen = false;
            }
            else
            {
                ShowSaveItemError(error);
            }
        }
        private void OnEditExecute(object obj)
        {
            if (obj != null)
            {
                SelectedItem = obj;
                IsNewEntry = false;
                IsPopupOpen = true;
                ItemsChanged = true;
            }
        }
        private void OnNewExecute(object obj)
        {
            string type = obj.ToString();
            switch (type)
            {
                case "Config": SelectedItem = new ConfigItem(); IsNewEntry = true; IsPopupOpen = true; break;
                case "Hydrant": SelectedItem = new Hydrant(); ((Hydrant)SelectedItem).GUID = Guid.NewGuid().ToString(); IsNewEntry = true; IsPopupOpen = true;  break;
                case "Settlement": SelectedItem = new Settlement(); ((Settlement)SelectedItem).GUID = Guid.NewGuid().ToString(); IsNewEntry = true; IsPopupOpen = true;  break;
            }
        }
        private void OnRemoveExecute(object obj)
        {
            if (obj != null)
            {
                Items.Remove(obj);
                ItemsChanged = true;
            }
        }
        private void OnSaveExecute(object obj)
        {
            Action_Save();
            ItemsChanged = false;
        }

        #endregion Execute

        #region CanExecute

        private bool OnSaveCanExecute(object arg)
        {
            if (arg != null)
            {
                return (bool)arg;
            }
            return false;
        }

        private bool OnRemoveCanExecute(object arg)
        {
            if (arg != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool OnNewCanExecute(object arg)
        {
            return true;
        }

        private bool OnEditCanExecute(object arg)
        {
            if (arg != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool OnSaveItemCanExecute(object arg)
        {
            return true;
        }

        #endregion CanExecute

        #endregion Commands

        #region Methods
        public abstract void Action_Save();

        protected abstract string CheckItemForSave(object obj);

        protected async void ShowSaveError(string error)
        {
            MessageDialog md = null;

            md = new MessageDialog(error, "Fehler!");
            UICommand uicOk = new UICommand("OK");
            md.Commands.Add(uicOk);

            await md.ShowAsync();
        }

        protected async void ShowSaveSuccess()
        {
            MessageDialog md = null;

            md = new MessageDialog("Daten wurden gespeichert!\nAnwendung sollte neu gestartet werden!","Erfolg!");
            UICommand uicOk = new UICommand("OK");
            md.Commands.Add(uicOk);

            await md.ShowAsync();
        }
        private async void ShowSaveItemError(string error)
        {
            MessageDialog md = null;

            md = new MessageDialog("Fehler beim speichern des Eintrages!\n"+error, "Fehler!");
            UICommand uicOk = new UICommand("OK");
            md.Commands.Add(uicOk);

            await md.ShowAsync();
        }

        #endregion Methods

    }
}
