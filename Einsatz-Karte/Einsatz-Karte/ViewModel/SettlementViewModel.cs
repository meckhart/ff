﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Einsatz_Karte.Model;

namespace Einsatz_Karte.ViewModel
{
    public class SettlementViewModel : BaseViewModel
    {
        private Settlement _Settlement;

        public Settlement Settlement
        {
            get { return _Settlement; }
            set { _Settlement = value; OnChanged(); }
        }
        
    }
}
