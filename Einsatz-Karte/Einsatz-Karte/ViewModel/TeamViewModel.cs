﻿using Einsatz_Karte.Model;
using Einsatz_Karte.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Einsatz_Karte.ViewModel
{
    public class TeamViewModel : BaseViewModel
    {
        public ICommand CommandActions { get; set; }

        private ObservableCollection<Firefighter> _Team;

        public ObservableCollection<Firefighter> Team
        {
            get { return _Team; }
            set { _Team = value; OnChanged(); ReportService.Team = Team; }
        }

        public TeamViewModel()
        {
            Team = new ObservableCollection<Firefighter>();
            LoadTeam();
            CommandActions = new ActionCommand(OnCommandActionsExecute, OnCommandActionsCanExecute);
        }

        private bool OnCommandActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnCommandActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "TeamToReport": break;
            }
        }

        private async void LoadTeam()
        {
            Team = new ObservableCollection<Firefighter>( await FileService.GetFirefighterXML());
        }



    }
}
