﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Einsatz_Karte.Model;
using System.Windows.Input;
using System.Collections.ObjectModel;
using Einsatz_Karte.Service;

namespace Einsatz_Karte.ViewModel
{
    public class ReportEditViewModel : BaseViewModel 
    {
        public event EventHandler ClearCan;
        public ICommand Actions { get; private set; }


        private string _Preview;

        public string Preview
        {
            get { return _Preview; }
            set { _Preview = value; OnChanged(); }
        }

        private Report _AlertReport;

        public Report AlertReport
        {
            get { return _AlertReport; }
            set { _AlertReport = value; OnChanged(); }
        }

        private bool _FirstPage;

        public bool FirstPage
        {
            get { return _FirstPage; }
            set { _FirstPage = value; OnChanged(); }
        }

        private bool _SecondPage;

        public bool SecondPage
        {
            get { return _SecondPage; }
            set { _SecondPage = value; OnChanged(); }
        }
        

        public ReportEditViewModel()
        {
            Actions = new ActionCommand(OnActionsExecute, OnActionsCanExecute);
            FirstPage = true;
            SecondPage = false;
            AlertReport = new Report();
        }


        #region Commands

        private bool OnActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {
                case "LoadDataFromPdf": LoadDataFromPdf(); break;
                case "OtherPage": ChangeSite(); break;
            }
        }

        #endregion

        internal void SetPreviewToTextBox(string field)
        {
            if (Preview != "")
            {
                switch (field)
                {
                    case "OtherAlert": AlertReport.OtherAlert = AlertReport.OtherAlert + Preview; Preview = ""; break;
                    case "UseLocation": AlertReport.UseLocation = AlertReport.UseLocation + Preview; Preview = ""; break;
                    case "UseStreet": AlertReport.UseStreet = AlertReport.UseStreet + Preview; Preview = ""; break;
                    case "MissionControlBrigade": AlertReport.MissionControlBrigade = AlertReport.MissionControlBrigade + Preview; Preview = ""; break;
                    case "Owner": AlertReport.Owner = AlertReport.Owner + Preview; Preview = ""; break;
                    case "PLZ": if(System.Text.RegularExpressions.Regex.IsMatch(Preview, @"^\d+$")){AlertReport.PLZ = Convert.ToInt32(Preview); Preview = "";} break;
                    case "OwnerLocation": AlertReport.OwnerLocation = AlertReport.OwnerLocation + Preview; Preview = ""; break;
                    case "LicensePlate": AlertReport.LicensePlate = AlertReport.LicensePlate + Preview; Preview = ""; break;
                    case "Vulgo": AlertReport.Vulgo = AlertReport.Vulgo + Preview; Preview = ""; break;
                    case "OwnerStreet": AlertReport.OwnerStreet = AlertReport.OwnerStreet + Preview; Preview = ""; break;
                    case "Info": AlertReport.Info = AlertReport.Info + Preview+"\n"; Preview = ""; break;
                }
            }
        }

        private async void LoadDataFromPdf()
        {
            AlertReport = await FileService.GetReportFromPdf();
        }

        private void ChangeSite()
        {
            if(FirstPage)
            {
                FirstPage = false;
                SecondPage = true;
            }
            else
            {
                FirstPage = true;
                SecondPage = false;
            }
        }
    }
}
