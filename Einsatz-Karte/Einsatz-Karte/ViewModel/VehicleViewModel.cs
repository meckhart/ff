﻿using Einsatz_Karte.Model;
using Einsatz_Karte.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Einsatz_Karte.ViewModel
{
    public class VehicleViewModel : BaseViewModel
    {
        public ICommand CommandActions { get; set; }
        public ICommand VehicleAdd { get; private set; }
        public ICommand VehicleRemove { get; private set; }

        private ObservableCollection<Vehicle> _Vehicles;
        public ObservableCollection<Vehicle> Vehicles
        {
            get { return _Vehicles; }
            set { _Vehicles = value; OnChanged(); ReportService.Vehicles = value; }
        }

        public VehicleViewModel()
        {
            Vehicles = new ObservableCollection<Vehicle>();
            CommandActions = new ActionCommand(OnCommandActionsExecute, OnCommandActionsCanExecute);
            VehicleAdd = new ActionCommand(OnVehicleAddActionsExecute, OnVehicleAddCanExecute);
            VehicleRemove = new ActionCommand(OnVehicleRemoceExecute, OnVehicleRemoceCanExecute);
            LoadVehicles();
        }

        public async void LoadVehicles()
        {
            Vehicles = new ObservableCollection<Vehicle>( await FileService.GetVehiclesXML());
        }

        private bool OnCommandActionsCanExecute(object arg)
        {
            return true;
        }

        private void OnCommandActionsExecute(object obj)
        {
            string action = obj.ToString();
            switch (action)
            {

            }
        }
        private bool OnVehicleRemoceCanExecute(object arg)
        {
            return true;
        }

        private void OnVehicleRemoceExecute(object obj)
        {
            string action = obj.ToString();
            Vehicles.First(p => p.Name.Equals(action)).KM--;

        }

        private bool OnVehicleAddCanExecute(object arg)
        {
            return true;
        }

        private void OnVehicleAddActionsExecute(object obj)
        {
            string action = obj.ToString();
            Vehicles.First(p => p.Name.Equals(action)).KM++;
        }

    }
}
